package service

import (
	"context"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LanguageService struct {
	pb.UnimplementedLanguageServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewLanguageService(strg storage.StorageI, log *logging.Logger) *LanguageService {
	return &LanguageService{
		storage: strg,
		logger:  log,
	}
}

func (s *LanguageService) Create(ctx context.Context, req *pb.Languages) (*pb.Languages, error) {
	var (
		languages pb.Languages
	)

	err := s.storage.Language().Delete(req.Languages[0].ApplicantId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all languages")
	}

	for _, v := range req.Languages {
		language, err := s.storage.Language().Create(&repo.Language{
			ApplicantId: v.ApplicantId,
			Language:    v.Language,
			Level:       v.Level,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new language")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		languages.Languages = append(languages.Languages, &pb.Language{
			Id:          language.ID,
			ApplicantId: language.ApplicantId,
			Language:    language.Language,
			Level:       language.Level,
		})
	}

	return &languages, nil
}

func (s *LanguageService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllLanguages, error) {
	languages, err := s.storage.Language().GetAll(&repo.GetAllLanguageParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all languages")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllLanguages{
		Languages: make([]*pb.Language, 0),
		Count:     languages.Count,
	}
	for _, language := range languages.Languages {
		p := parseLanguage(language)
		res.Languages = append(res.Languages, p)
	}
	return &res, nil
}

func parseLanguage(req *repo.Language) *pb.Language {
	return &pb.Language{
		Id:          req.ID,
		ApplicantId: req.ApplicantId,
		Language:    req.Language,
		Level:       req.Level,
	}
}
