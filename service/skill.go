package service

import (
	"context"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SkillService struct {
	pb.UnimplementedSkillServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewSkillService(strg storage.StorageI, log *logging.Logger) *SkillService {
	return &SkillService{
		storage: strg,
		logger:  log,
	}
}

func (s *SkillService) Create(ctx context.Context, req *pb.ApplicantSkills) (*pb.ApplicantSkills, error) {
	var (
		skills pb.ApplicantSkills
	)

	err := s.storage.Skill().Delete(req.Skills[0].ApplicantId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all skills")
	}

	for _, v := range req.Skills {
		skill, err := s.storage.Skill().Create(&repo.Skill{
			ApplicantId: v.ApplicantId,
			Name:        v.Name,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new language")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		skills.Skills = append(skills.Skills, &pb.Skill{
			Id:          skill.ID,
			ApplicantId: skill.ApplicantId,
			Name:        skill.Name,
		})
	}

	return &skills, nil
}

func (s *SkillService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllSkills, error) {
	skills, err := s.storage.Skill().GetAll(&repo.GetAllSkillParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all skills")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllSkills{
		Skills: make([]*pb.Skill, 0),
		Count:  skills.Count,
	}
	for _, skill := range skills.Skills {
		p := parseSkill(skill)
		res.Skills = append(res.Skills, p)
	}
	return &res, nil
}

func parseSkill(req *repo.Skill) *pb.Skill {
	return &pb.Skill{
		Id:          req.ID,
		ApplicantId: req.ApplicantId,
		Name:        req.Name,
	}
}
