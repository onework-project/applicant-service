package service

import (
	"context"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SocialMediaService struct {
	pb.UnimplementedSocialMediaServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewSocialMediaService(strg storage.StorageI, log *logging.Logger) *SocialMediaService {
	return &SocialMediaService{
		storage: strg,
		logger:  log,
	}
}

func (s *SocialMediaService) Create(ctx context.Context, req *pb.ApplicantSocialMedias) (*pb.ApplicantSocialMedias, error) {
	var (
		socialMedias pb.ApplicantSocialMedias
	)

	err := s.storage.SocialMedia().Delete(req.SocialMedias[0].ApplicantId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete all social medias")
	}

	for _, v := range req.SocialMedias {
		socialMedia, err := s.storage.SocialMedia().Create(&repo.SocialMedia{
			ApplicantId: v.ApplicantId,
			Name:        v.Name,
			Url:         v.Url,
		})
		if err != nil {
			s.logger.WithError(err).Error("failed to create new language")
			return nil, status.Errorf(codes.Internal, "internal server error")
		}
		socialMedias.SocialMedias = append(socialMedias.SocialMedias, &pb.SocialMedia{
			Id:          socialMedia.ID,
			ApplicantId: socialMedia.ApplicantId,
			Name:        socialMedia.Name,
			Url:         socialMedia.Url,
		})
	}

	return &socialMedias, nil
}

func (s *SocialMediaService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllSocialMediasRes, error) {
	socialMedias, err := s.storage.SocialMedia().GetAll(&repo.GetAllSocialMediasParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all SocialMedias")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllSocialMediasRes{
		SocialMedias: make([]*pb.SocialMedia, 0),
		Count:        socialMedias.Count,
	}
	for _, socialMedia := range socialMedias.SocialMedias {
		p := parseSocialMedia(socialMedia)
		res.SocialMedias = append(res.SocialMedias, p)
	}
	return &res, nil
}

func parseSocialMedia(req *repo.SocialMedia) *pb.SocialMedia {
	return &pb.SocialMedia{
		Id:          req.ID,
		ApplicantId: req.ApplicantId,
		Name:        req.Name,
		Url:         req.Url,
	}
}
