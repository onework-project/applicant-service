package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	pbc "gitlab.com/onework-project/applicant-service/genproto/company_service"
	grpcPkg "gitlab.com/onework-project/applicant-service/pkg/grpc_client"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BlockedCompanyService struct {
	pb.UnimplementedBlockedCompanyServiceServer
	storage storage.StorageI
	logger  *logging.Logger
	grpcCon grpcPkg.GrpcClientI
}

func NewBlockedCompany(strg storage.StorageI, log *logging.Logger, grpcCon grpcPkg.GrpcClientI) *BlockedCompanyService {
	return &BlockedCompanyService{
		storage: strg,
		logger:  log,
		grpcCon: grpcCon,
	}
}

func (s *BlockedCompanyService) Create(ctx context.Context, req *pb.BlockedCompany) (*pb.BlockedCompany, error) {
	blockedEmployer, err := s.storage.BlockedCompany().Create(&repo.BlockedCompany{
		ApplicantID: req.ApplicantId,
		CompanyID:   req.CompanyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new blocked company")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	companyInfo, err := s.grpcCon.CompanyService().Get(context.Background(), &pbc.CompanyIDReq{
		Id: blockedEmployer.CompanyID,
	})
	// * if err acquires just continue to next blocked company info
	if err != nil {
		s.logger.WithError(err).Error("failed to get company info")
	}

	return parseBlockedCompany(blockedEmployer, companyInfo), nil
}

func (s *BlockedCompanyService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.BlockedCompany().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete blocked company")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "blocked company does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *BlockedCompanyService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllBlockedCompaniesRes, error) {
	blockedCompanies, err := s.storage.BlockedCompany().GetAll(&repo.GetAllBlockedCompaniesParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all blocked companies")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	res := pb.GetAllBlockedCompaniesRes{
		BlockedCompanies: make([]*pb.BlockedCompany, 0),
		Count:            blockedCompanies.Count,
	}
	for _, blockedEmployer := range blockedCompanies.BlockedCompanies {
		companyInfo, err := s.grpcCon.CompanyService().Get(context.Background(), &pbc.CompanyIDReq{
			Id: blockedEmployer.CompanyID,
		})
		// * if err acquires just continue to next blocked company info
		if err != nil {
			continue
		}
		res.BlockedCompanies = append(res.BlockedCompanies, parseBlockedCompany(blockedEmployer, companyInfo))
	}
	return &res, nil
}

func parseBlockedCompany(req *repo.BlockedCompany, companyInfo *pbc.Company) *pb.BlockedCompany {
	return &pb.BlockedCompany{
		Id:          req.ID,
		ApplicantId: req.ApplicantID,
		CompanyId:   req.CompanyID,
		CompanyInfo: &pb.CompanyInfo{
			CompanyName:  companyInfo.CompanyName,
			Description:  companyInfo.Description,
			Email:        companyInfo.Email,
			PhoneNumber:  companyInfo.PhoneNumber,
			Country:      companyInfo.Country,
			RegionState:  companyInfo.RegionState,
			WorkersCount: companyInfo.WorkersCount,
			ImageUrl:     companyInfo.ImageUrl,
			CreatedAt:    companyInfo.CreatedAt,
			Slug:         companyInfo.Slug,
		},
		CreatedAt: req.CreatedAt.Format(time.RFC3339),
	}
}
