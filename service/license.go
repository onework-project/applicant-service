package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LicenseService struct {
	pb.UnimplementedLicenseServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewLicenseService(strg storage.StorageI, log *logging.Logger) *LicenseService {
	return &LicenseService{
		storage: strg,
		logger:  log,
	}
}

func (s *LicenseService) Create(ctx context.Context, req *pb.License) (*pb.License, error) {
	license, err := s.storage.License().Create(&repo.License{
		ApplicantId:  req.ApplicantId,
		Name:         req.Name,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new license")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseLicense(license), nil
}

func (s *LicenseService) Get(ctx context.Context, req *pb.GetReq) (*pb.License, error) {
	license, err := s.storage.License().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get license")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "licence does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseLicense(license), nil
}

func (s *LicenseService) Update(ctx context.Context, req *pb.License) (*pb.License, error) {
	License, err := s.storage.License().Update(&repo.License{
		ID:           req.Id,
		ApplicantId:  req.ApplicantId,
		Name:         req.Name,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update license")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "licence does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseLicense(License), nil
}

func (s *LicenseService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.License().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete license")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "licence does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *LicenseService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllLicenses, error) {
	licenses, err := s.storage.License().GetAll(&repo.GetAllLicenseParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all licenses")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllLicenses{
		Licenses: make([]*pb.License, 0),
		Count:    licenses.Count,
	}
	for _, license := range licenses.Licenses {
		p := parseLicense(license)
		res.Licenses = append(res.Licenses, p)
	}
	return &res, nil
}

func parseLicense(req *repo.License) *pb.License {
	return &pb.License{
		Id:           req.ID,
		ApplicantId:  req.ApplicantId,
		Name:         req.Name,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	}
}
