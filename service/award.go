package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AwardService struct {
	pb.UnimplementedAwardServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewAwardService(strg storage.StorageI, log *logging.Logger) *AwardService {
	return &AwardService{
		storage: strg,
		logger:  log,
	}
}

func (s *AwardService) Create(ctx context.Context, req *pb.Award) (*pb.Award, error) {
	award, err := s.storage.Award().Create(&repo.Award{
		ApplicantId:  req.ApplicantId,
		AwardName:    req.AwardName,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new award")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAward(award), nil
}

func (s *AwardService) Get(ctx context.Context, req *pb.GetReq) (*pb.Award, error) {
	award, err := s.storage.Award().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get award")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "award does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAward(award), nil
}

func (s *AwardService) Update(ctx context.Context, req *pb.Award) (*pb.Award, error) {
	award, err := s.storage.Award().Update(&repo.Award{
		ID:           req.Id,
		ApplicantId:  req.ApplicantId,
		AwardName:    req.AwardName,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update award")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "award does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAward(award), nil
}

func (s *AwardService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.Award().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete award")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "award does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *AwardService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllAwards, error) {
	awards, err := s.storage.Award().GetAll(&repo.GetAllAwardParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all awards")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllAwards{
		Awards: make([]*pb.Award, 0),
		Count:  awards.Count,
	}
	for _, award := range awards.Awards {
		p := parseAward(award)
		res.Awards = append(res.Awards, p)
	}
	return &res, nil
}

func parseAward(req *repo.Award) *pb.Award {
	return &pb.Award{
		Id:           req.ID,
		ApplicantId:  req.ApplicantId,
		AwardName:    req.AwardName,
		Organization: req.Organization,
		PdfUrl:       req.PdfUrl,
	}
}
