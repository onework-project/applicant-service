package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type WorkExpService struct {
	pb.UnimplementedWorkExpServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewWorkExpService(strg storage.StorageI, log *logging.Logger) *WorkExpService {
	return &WorkExpService{
		storage: strg,
		logger:  log,
	}
}

func (s *WorkExpService) Create(ctx context.Context, req *pb.WorkExp) (*pb.WorkExp, error) {
	workExp, err := s.storage.Work().Create(&repo.Work{
		ApplicantId:    req.ApplicantId,
		CompanyName:    req.CompanyName,
		WorkPosition:   req.WorkPosition,
		CompanyWebsite: req.CompanyWebsite,
		EmploymentType: req.EmploymentType,
		StartYear:      req.StartYear,
		StartMonth:     req.StartMonth,
		EndYear:        req.EndYear,
		EndMonth:       req.EndMonth,
		Description:    req.Description,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new work experience")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseWorkExp(workExp), nil
}

func (s *WorkExpService) Get(ctx context.Context, req *pb.GetReq) (*pb.WorkExp, error) {
	workExp, err := s.storage.Work().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get work experience")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "work experience does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseWorkExp(workExp), nil
}

func (s *WorkExpService) Update(ctx context.Context, req *pb.WorkExp) (*pb.WorkExp, error) {
	WorkExp, err := s.storage.Work().Update(&repo.Work{
		ID:             req.Id,
		ApplicantId:    req.ApplicantId,
		CompanyName:    req.CompanyName,
		WorkPosition:   req.WorkPosition,
		CompanyWebsite: req.CompanyWebsite,
		EmploymentType: req.EmploymentType,
		StartYear:      req.StartYear,
		StartMonth:     req.StartMonth,
		EndYear:        req.EndYear,
		EndMonth:       req.EndMonth,
		Description:    req.Description,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update work experience")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "work experience does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseWorkExp(WorkExp), nil
}

func (s *WorkExpService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.Work().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete work experience")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "work experience does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *WorkExpService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllWorkExps, error) {
	workExps, err := s.storage.Work().GetAll(&repo.GetAllWorkExpParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all work experience")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllWorkExps{
		WorkExps: make([]*pb.WorkExp, 0),
		Count:    workExps.Count,
	}
	for _, workExp := range workExps.WorkExps {
		p := parseWorkExp(workExp)
		res.WorkExps = append(res.WorkExps, p)
	}
	return &res, nil
}

func parseWorkExp(req *repo.Work) *pb.WorkExp {
	return &pb.WorkExp{
		Id:             req.ID,
		ApplicantId:    req.ApplicantId,
		CompanyName:    req.CompanyName,
		WorkPosition:   req.WorkPosition,
		CompanyWebsite: req.CompanyWebsite,
		EmploymentType: req.EmploymentType,
		StartYear:      req.StartYear,
		StartMonth:     req.StartMonth,
		EndYear:        req.EndYear,
		EndMonth:       req.EndMonth,
		Description:    req.Description,
	}
}
