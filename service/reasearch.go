package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ResearchService struct {
	pb.UnimplementedResearchServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewResearchService(strg storage.StorageI, log *logging.Logger) *ResearchService {
	return &ResearchService{
		storage: strg,
		logger:  log,
	}
}

func (s *ResearchService) Create(ctx context.Context, req *pb.Research) (*pb.Research, error) {
	research, err := s.storage.Research().Create(&repo.Research{
		ApplicantID:      req.ApplicantId,
		OrganizationName: req.OrganizationName,
		Position:         req.Position,
		Description:      req.Description,
		Supervisor:       req.Supervisor,
		SupervisorEmail:  req.SupervisorEmail,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new research")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseResearch(research), nil
}

func (s *ResearchService) Get(ctx context.Context, req *pb.GetReq) (*pb.Research, error) {
	research, err := s.storage.Research().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get research")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "research does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseResearch(research), nil
}

func (s *ResearchService) Update(ctx context.Context, req *pb.Research) (*pb.Research, error) {
	research, err := s.storage.Research().Update(&repo.Research{
		ID:               req.Id,
		ApplicantID:      req.ApplicantId,
		OrganizationName: req.OrganizationName,
		Position:         req.Position,
		Description:      req.Description,
		Supervisor:       req.Supervisor,
		SupervisorEmail:  req.SupervisorEmail,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update research")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "research does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseResearch(research), nil
}

func (s *ResearchService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.Research().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete research")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "research does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *ResearchService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllResearches, error) {
	researches, err := s.storage.Research().GetAll(&repo.GetAllResearchParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all researchs")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllResearches{
		Researches: make([]*pb.Research, 0),
		Count:      researches.Count,
	}
	for _, research := range researches.Researches {
		p := parseResearch(research)
		res.Researches = append(res.Researches, p)
	}
	return &res, nil
}

func parseResearch(req *repo.Research) *pb.Research {
	return &pb.Research{
		Id:               req.ID,
		ApplicantId:      req.ApplicantID,
		OrganizationName: req.OrganizationName,
		Position:         req.Position,
		Description:      req.Description,
		Supervisor:       req.Supervisor,
		SupervisorEmail:  req.SupervisorEmail,
	}
}
