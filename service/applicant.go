package service

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/gosimple/slug"
	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/grpc_client"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ApplicantService struct {
	pb.UnimplementedApplicantServiceServer
	storage    storage.StorageI
	logger     *logging.Logger
	grpcClient grpc_client.GrpcClientI
	tracer     trace.Tracer
}

func NewApplicantService(strg storage.StorageI, log *logging.Logger, grpcClient grpc_client.GrpcClientI, tracer trace.Tracer) *ApplicantService {
	return &ApplicantService{
		storage:    strg,
		logger:     log,
		grpcClient: grpcClient,
		tracer:     tracer,
	}
}

func (s *ApplicantService) Create(ctx context.Context, req *pb.CreateApplicantReq) (*pb.Applicant, error) {
	ctx, span := s.tracer.Start(ctx, "service.CreateApplicant")
	defer span.End()

	slugIs := slug.Make(fmt.Sprintf("%s %s", req.FirstName, req.LastName))
	id, err := s.storage.Applicant().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != 0 {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %s %d", req.FirstName, req.LastName, timestamp))
	}

	applicant, err := s.storage.Applicant().Create(ctx, &repo.Applicant{
		ID:           req.Id,
		FirstName:    req.FirstName,
		LastName:     req.LastName,
		Speciality:   req.Speciality,
		Country:      req.Country,
		City:         req.City,
		DateOfBirth:  req.DateOfBirth,
		InVisibleAge: req.InvisibleAge,
		PhoneNumber:  req.PhoneNumber,
		Email:        req.Email,
		About:        req.About,
		ImageUrl:     req.ImageUrl,
		Slug:         slugIs,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new applicant")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseApplicant(applicant), nil
}

func (s *ApplicantService) Get(ctx context.Context, req *pb.IdRequest) (*pb.Applicant, error) {
	applicant, err := s.storage.Applicant().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get applicant")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseApplicant(applicant), nil
}

func (s *ApplicantService) Update(ctx context.Context, req *pb.Applicant) (*pb.Applicant, error) {
	slugIs := slug.Make(fmt.Sprintf("%s %s", req.FirstName, req.LastName))
	id, err := s.storage.Applicant().GetSlug(slugIs)
	if !errors.Is(err, sql.ErrNoRows) && id != req.Id {
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		slugIs = slug.Make(fmt.Sprintf("%s %s %d", req.FirstName, req.LastName, timestamp))
	}

	applicant, err := s.storage.Applicant().Update(&repo.Applicant{
		ID:           req.Id,
		FirstName:    req.FirstName,
		LastName:     req.LastName,
		Speciality:   req.Speciality,
		Country:      req.Country,
		City:         req.City,
		DateOfBirth:  req.DateOfBirth,
		InVisibleAge: req.InvisibleAge,
		PhoneNumber:  req.PhoneNumber,
		About:        req.About,
		ImageUrl:     req.ImageUrl,
		Slug:         slugIs,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update applicant")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseApplicant(applicant), nil
}

func (s *ApplicantService) UpdateStepField(ctx context.Context, req *pb.UpdateStepReq) (*pb.Applicant, error) {
	applicant, err := s.storage.Applicant().UpdateStepField(req.Arg, req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to update applicant")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseApplicant(applicant), nil
}

func (s *ApplicantService) UpdateEmail(ctx context.Context, req *pb.UpdateApplicantEmailReq) (*pb.Empty, error) {
	err := s.storage.Applicant().UpdateEmail(req.Email, req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to update applicant")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *ApplicantService) Delete(ctx context.Context, req *pb.IdRequest) (*pb.Empty, error) {
	err := s.storage.Applicant().Delete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete applicant")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "applicant does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *ApplicantService) GetAll(ctx context.Context, req *pb.GetAllApplicantsParams) (*pb.GetAllApplicantsRes, error) {
	applicants, err := s.storage.Applicant().GetAll(&repo.GetAllApplicantParams{
		Limit:               req.Limit,
		Page:                req.Page,
		Languages:           req.Languages,
		Educations:          req.Educations,
		Skills:              req.Skills,
		LicenceCertificates: req.Licences,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all applicants")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	res := pb.GetAllApplicantsRes{
		Applicants: make([]*pb.Applicant, 0),
		Count:      applicants.Count,
	}
	for _, applicant := range applicants.Applicants {
		p := parseApplicant(applicant)
		res.Applicants = append(res.Applicants, p)
	}
	return &res, nil
}

func parseApplicant(req *repo.Applicant) *pb.Applicant {
	return &pb.Applicant{
		Id:           req.ID,
		FirstName:    req.FirstName,
		LastName:     req.LastName,
		Speciality:   req.Speciality,
		Country:      req.Country,
		City:         req.City,
		DateOfBirth:  req.DateOfBirth,
		InvisibleAge: req.InVisibleAge,
		PhoneNumber:  req.PhoneNumber,
		Email:        req.Email,
		About:        req.About,
		ImageUrl:     req.ImageUrl,
		LastStep:     req.LastStep,
		CreatedAt:    req.CreatedAt.Format(time.RFC3339),
		UpdatedAt:    req.UpdatedAt.Format(time.RFC3339),
		Slug:         req.Slug,
	}
}
