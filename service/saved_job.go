package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	pbc "gitlab.com/onework-project/applicant-service/genproto/company_service"
	grpcPkg "gitlab.com/onework-project/applicant-service/pkg/grpc_client"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SavedJobService struct {
	pb.UnimplementedSavedJobServiceServer
	storage storage.StorageI
	logger  *logging.Logger
	grpcCon grpcPkg.GrpcClientI
}

func NewSavedJob(strg storage.StorageI, log *logging.Logger, grpcCon grpcPkg.GrpcClientI) *SavedJobService {
	return &SavedJobService{
		storage: strg,
		logger:  log,
		grpcCon: grpcCon,
	}
}

func (s *SavedJobService) Create(ctx context.Context, req *pb.SavedJob) (*pb.SavedJob, error) {
	savedJob, err := s.storage.SavedJob().Create(&repo.SavedJob{
		ApplicantID: req.ApplicantId,
		VacancyID:   req.VacancyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new saved job")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}
	companyInfo, vacancyInfo := s.getVacancyAndCompanyInfo(savedJob.VacancyID)

	return parseSavedJob(savedJob, companyInfo, vacancyInfo), nil
}

func (s *SavedJobService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.SavedJob().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete saved job")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "saved job does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *SavedJobService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllSavedJobsRes, error) {
	savedJobs, err := s.storage.SavedJob().GetAll(&repo.GetAllSavedJobsParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all blocked companies")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllSavedJobsRes{
		SavedJobs: make([]*pb.SavedJob, 0),
		Count:     savedJobs.Count,
	}
	for _, savedJob := range savedJobs.SavedJobs {
		companyInfo, vacancyInfo := s.getVacancyAndCompanyInfo(savedJob.VacancyID)
		if companyInfo == nil || vacancyInfo == nil {
			continue
		}

		res.SavedJobs = append(res.SavedJobs, parseSavedJob(savedJob, companyInfo, vacancyInfo))
	}
	return &res, nil
}

func (s *SavedJobService) getVacancyAndCompanyInfo(vacancyId int64) (*pbc.Company, *pbc.Vacancy) {
	vacancyInfo, _ := s.grpcCon.VacancyService().Get(context.Background(), &pbc.GetVacancyReq{
		Id: vacancyId,
	})

	companyInfo, _ := s.grpcCon.CompanyService().Get(context.Background(), &pbc.CompanyIDReq{
		Id: vacancyInfo.CompanyId,
	})

	return companyInfo, vacancyInfo
}

func parseSavedJob(req *repo.SavedJob, companyInfo *pbc.Company, vacancyInfo *pbc.Vacancy) *pb.SavedJob {
	return &pb.SavedJob{
		Id:          req.ID,
		ApplicantId: req.ApplicantID,
		VacancyId:   req.VacancyID,
		VacancyInfo: &pb.VacancyInfo{
			CompanyId: vacancyInfo.CompanyId,
			CompanyInfo: &pb.CompanyInfo{
				CompanyName:  companyInfo.CompanyName,
				Description:  companyInfo.Description,
				Email:        companyInfo.Email,
				PhoneNumber:  companyInfo.PhoneNumber,
				Country:      companyInfo.Country,
				RegionState:  companyInfo.RegionState,
				WorkersCount: companyInfo.WorkersCount,
				ImageUrl:     companyInfo.ImageUrl,
				CreatedAt:    companyInfo.CreatedAt,
				Slug:         companyInfo.Slug,
			},
			Title:          vacancyInfo.Title,
			Deadline:       vacancyInfo.Deadline,
			EmploymentForm: vacancyInfo.EmploymentForm,
			EmploymentType: vacancyInfo.EmploymentType,
			Country:        vacancyInfo.Country,
			City:           vacancyInfo.City,
			SalaryMin:      vacancyInfo.SalaryMin,
			SalaryMax:      vacancyInfo.SalaryMax,
			Currency:       vacancyInfo.Currency,
			SalaryPeriod:   vacancyInfo.SalaryPeriod,
			Education:      vacancyInfo.Education,
			Experience:     vacancyInfo.Experience,
			Description:    vacancyInfo.Description,
			ViewsCount:     vacancyInfo.ViewsCount,
			CreatedAt:      vacancyInfo.CreatedAt,
			UpdatedAt:      vacancyInfo.UpdatedAt,
			Slug:           vacancyInfo.Slug,
		},
		CreatedAt: req.CreatedAt.Format(time.RFC3339),
	}
}
