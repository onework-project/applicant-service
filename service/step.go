package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StepService struct {
	pb.UnimplementedStepServiceServer
	storage storage.StorageI
	logger  *logging.Logger
	tracer  trace.Tracer
}

func NewStepService(strg storage.StorageI, log *logging.Logger, tracer trace.Tracer) *StepService {
	return &StepService{
		storage: strg,
		logger:  log,
		tracer:  tracer,
	}
}

func (s *StepService) Create(ctx context.Context, req *pb.IdRequest) (*pb.Step, error) {
	_, span := s.tracer.Start(ctx, "service.CreateApplicantStep")
	defer span.End()
	
	step, err := s.storage.Step().Create(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to create step")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseStep(step), nil
}

func (s *StepService) Get(ctx context.Context, req *pb.IdRequest) (*pb.Step, error) {
	step, err := s.storage.Step().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to create step")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "step not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseStep(step), nil
}

func (s *StepService) UpdateField(ctx context.Context, req *pb.UpdateStepField) (*pb.Step, error) {
	step, err := s.storage.Step().UpdateField(req.Field, req.Arg, req.ApplicantId)
	if err != nil {
		s.logger.WithError(err).Error("failed to update step")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "step does not exist")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseStep(step), nil
}

func (s *StepService) Delete(ctx context.Context, req *pb.IdRequest) (*pb.Empty, error) {
	err := s.storage.Step().Delete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete step")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "step does not exist")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func parseStep(req *repo.Step) *pb.Step {
	return &pb.Step{
		Id:                  req.ID,
		ApplicantId:         req.ApplicantID,
		GeneralInfo:         req.GeneralInfo,
		Work:                req.Work,
		Education:           req.Education,
		AcademicTest:        req.AcademicTests,
		LicenceCertificates: req.LicenceCertificates,
		Award:               req.Award,
		Research:            req.Research,
	}
}
