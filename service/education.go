package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EducationService struct {
	pb.UnimplementedEducationServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewEducationService(strg storage.StorageI, log *logging.Logger) *EducationService {
	return &EducationService{
		storage: strg,
		logger:  log,
	}
}

func (s *EducationService) Create(ctx context.Context, req *pb.Education) (*pb.Education, error) {
	education, err := s.storage.Education().Create(&repo.Education{
		ApplicantID:    req.ApplicantId,
		University:     req.University,
		DegreeName:     req.DegreeName,
		DegreeLevel:    req.DegreeLevel,
		StartYear:      req.StartYear,
		EndYear:        req.EndYear,
		ApplicantGrade: req.ApplicantGrade,
		MaxGrade:       req.MaxGrade,
		PdfUrl:         req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new education")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseEducation(education), nil
}

func (s *EducationService) Get(ctx context.Context, req *pb.GetReq) (*pb.Education, error) {
	education, err := s.storage.Education().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get education")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "education does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseEducation(education), nil
}

func (s *EducationService) Update(ctx context.Context, req *pb.Education) (*pb.Education, error) {
	education, err := s.storage.Education().Update(&repo.Education{
		ID:             req.Id,
		ApplicantID:    req.ApplicantId,
		University:     req.University,
		DegreeName:     req.DegreeName,
		DegreeLevel:    req.DegreeLevel,
		StartYear:      req.StartYear,
		EndYear:        req.EndYear,
		ApplicantGrade: req.ApplicantGrade,
		MaxGrade:       req.MaxGrade,
		PdfUrl:         req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update education")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "education does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseEducation(education), nil
}

func (s *EducationService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.Education().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete education")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "education does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *EducationService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllEdus, error) {
	educations, err := s.storage.Education().GetAll(&repo.GetAllEduParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all educations")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllEdus{
		Educations: make([]*pb.Education, 0),
		Count:      educations.Count,
	}
	for _, education := range educations.Educations {
		p := parseEducation(education)
		res.Educations = append(res.Educations, p)
	}
	return &res, nil
}

func parseEducation(req *repo.Education) *pb.Education {
	return &pb.Education{
		Id:             req.ID,
		ApplicantId:    req.ApplicantID,
		University:     req.University,
		DegreeName:     req.DegreeName,
		DegreeLevel:    req.DegreeLevel,
		StartYear:      req.StartYear,
		EndYear:        req.EndYear,
		ApplicantGrade: req.ApplicantGrade,
		MaxGrade:       req.MaxGrade,
		PdfUrl:         req.PdfUrl,
	}
}
