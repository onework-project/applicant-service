package service

import (
	"context"
	"database/sql"
	"errors"

	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/storage"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AcademicService struct {
	pb.UnimplementedAcademicServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewAcademicService(strg storage.StorageI, log *logging.Logger) *AcademicService {
	return &AcademicService{
		storage: strg,
		logger:  log,
	}
}

func (s *AcademicService) Create(ctx context.Context, req *pb.Academic) (*pb.Academic, error) {
	academic, err := s.storage.Academic().Create(&repo.Academic{
		ApplicantID:  req.ApplicantId,
		TestName:     req.TestName,
		Organization: req.Organization,
		Score:        float64(req.Score),
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create new academic in create func")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAcademic(academic), nil
}

func (s *AcademicService) Get(ctx context.Context, req *pb.GetReq) (*pb.Academic, error) {
	academic, err := s.storage.Academic().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get academic in get func")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "academic does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAcademic(academic), nil
}

func (s *AcademicService) Update(ctx context.Context, req *pb.Academic) (*pb.Academic, error) {
	academic, err := s.storage.Academic().Update(&repo.Academic{
		ID:           req.Id,
		ApplicantID:  req.ApplicantId,
		TestName:     req.TestName,
		Organization: req.Organization,
		Score:        float64(req.Score),
		PdfUrl:       req.PdfUrl,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update academic in update func")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "academic does not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return parseAcademic(academic), nil
}

func (s *AcademicService) Delete(ctx context.Context, req *pb.DeleteOrGetReq) (*pb.Empty, error) {
	err := s.storage.Academic().Delete(req.Id, req.ThingId)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete academic")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "academic not exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.Empty{}, nil
}

func (s *AcademicService) GetAll(ctx context.Context, req *pb.GetAllParams) (*pb.GetAllAcademicResults, error) {
	academics, err := s.storage.Academic().GetAll(&repo.GetAllAcademicsParams{
		Limit:       req.Limit,
		Page:        req.Page,
		ApplicantID: req.ApplicantId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all academics")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	res := pb.GetAllAcademicResults{
		Academics: make([]*pb.Academic, 0),
		Count:     academics.Count,
	}
	for _, academic := range academics.Academics {
		p := parseAcademic(academic)
		res.Academics = append(res.Academics, p)
	}
	return &res, nil
}

func parseAcademic(req *repo.Academic) *pb.Academic {
	return &pb.Academic{
		Id:           req.ID,
		ApplicantId:  req.ApplicantID,
		TestName:     req.TestName,
		Organization: req.Organization,
		Score:        float32(req.Score),
		PdfUrl:       req.PdfUrl,
	}
}
