package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	GrpcPort               string
	Postgres               PostgresConfig
	Jaeger                 Jaeger
	CompanyServiceHost     string
	CompanyServiceGrpcPort string
}

type Jaeger struct {
	Host        string
	ServiceName string
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		GrpcPort: conf.GetString("GRPC_PORT"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
		Jaeger: Jaeger{
			Host:        conf.GetString("JAEGER_HOST"),
			ServiceName: conf.GetString("JAEGER_SERVICE_NAME"),
		},
		CompanyServiceHost:     conf.GetString("COMPANY_SERVICE_HOST"),
		CompanyServiceGrpcPort: conf.GetString("COMPANY_SERVICE_GRPC_PORT"),
	}
	return cfg
}
