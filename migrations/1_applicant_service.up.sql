-- * this table to store applicant information 
create table if not exists "applicants" (
    "id" int primary key,
    "first_name" varchar(50) not null,
    "last_name" varchar(50) not null,
    "speciality" varchar not null,
    "country" varchar not null,
    "city" varchar not null,
    "date_of_birth" varchar(15) not null,
    "invisible_age" bool default false,
    "phone_number" varchar(50) not null,
    "email" varchar(100) not null,
    "about" text,
    "image_url" text,
    "last_step" varchar check ("last_step" in ('general_info', 'work', 'education', 'academic_tests', 'licences_certificates', 'awards', 'research', 'completed')),
    "created_at" timestamp with time zone default current_timestamp,
    "updated_at" timestamp with time zone,
    unique(email, phone_number)
);

-- * applicants social media table (If have)
create table if not exists "social_medias" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "name" varchar(50) not null  check ("name" in ('website', 'linkedin', 'facebook', 'github', 'instagram', 'telegram', 'youtube')),
    "url" text not null,
    unique(applicant_id, name)
);

-- * applicants worked experiences 
create table if not exists "work_experiences" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "company_name" varchar(100) not null,
    "work_position" varchar(100) not null,
    "company_website" varchar,
    "employment_type" varchar(50) not null check ("employment_type" in ('full-time', 'part-time', 'freelance', 'contract', 'internship', 'apprenticeship', 'seasonal')),
    "start_year" varchar(5) not null,
    "start_month" varchar(15) not null,
    "end_year" varchar(5), -- * if row is null, it looks like that applicant is working there
    "end_month" varchar(15), -- * if row is null, it looks like that applicant is working there
    "description" text
);

-- * applicants education (IF applicant has)
create table if not exists "educations" (
    "id" serial primary key,
    "applicant_id" int REFERENCES applicants(id) on delete cascade,
    "university" varchar(100) not null,
    "degree_name" varchar(50) not null,
    "degree_level" varchar(50) not null check ("degree_level" in ('high-school-or-equivalent', 'certification', 'vocational', 'associate', 'bachelor', 'master', 'doctorate')),
    "start_year" varchar(5) not null,
    "end_year" varchar(5) not null,
    "applicant_grade" int check ("applicant_grade" > 0),
    "max_grade" int check ("max_grade" > 0),
    "pdf_url" text
);

-- * this table to store applicant  language (Required) one more than
create table if not exists "languages" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "language" varchar(50) not null,
    "level" varchar(30) not null check ("level" in ('a1', 'a2', 'b1', 'b2', 'c1', 'c2', 'n'))
);

-- * this table to store applicant skills (Required) one more than
create table if not exists "skills" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "name" varchar not null
);

-- * this table to store applicants academic test results optional for SAT, IELTS, TOEFL, and so on (ADDITIONAL)
create table if not exists "academic_test_results" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "test_name" varchar(100) not null,
    "organization" varchar(100) not null,
    "score" numeric(4, 2) not null,
    -- * optional it can be null 
    "pdf_url" text 
);

-- * this table to store applicants licences and sertificates from organizations (ADDITIONAL)
create table if not exists "licenses_and_sertificates" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "name" varchar not null,
    "organization" varchar not null,
    -- * optional it can be null 
    "pdf_url" text 
);
 
-- * this table to store applicants researches (ADDITIONAL)
create table if not exists "researches" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "organization_name" varchar(100) not null,
    "position" varchar(100) not null,
    "description" text not null,
    "superviser" varchar(60) not null,
    "supervisor_email" varchar(100)
);

-- * this table to store applicants awards (ADDITIONAL)
create table if not exists "awards" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) on delete cascade,
    "award_name" varchar(100) not null,
    "organization" varchar(100) not null,
    "pdf_url" text 
);

create table if not exists "blocked_companies" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) ON DELETE CASCADE,
    "company_id" int not null check ("company_id" > 0),
    "created_at" timestamp with time zone default current_timestamp,
    unique(applicant_id, company_id)
);

create table if not exists "saved_jobs" (
    "id" serial primary key,
    "applicant_id" int not null REFERENCES applicants(id) ON DELETE CASCADE,
    "vacancy_id" int not null check ("vacancy_id" > 0),
    "created_at" timestamp with time zone default current_timestamp,
    unique(applicant_id, vacancy_id)
);

create table if not exists "steps" (
    "id" serial primary key,
    "applicant_id" int not null,
    "general_info" boolean default false,
    "work" boolean default false,
    "education" boolean default false,
    "academic_tests" boolean default false,
    "licences_certificates" boolean default false,
    "awards" boolean default false,
    "research" boolean default false
);