package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createAward(t *testing.T) *repo.Award {
	applicant := createApplicant(t)
	res, err := dbManager.Award().Create(&repo.Award{
		ApplicantId:  applicant.ID,
		AwardName:    faker.Word(),
		Organization: faker.Sentence(),
		PdfUrl:       faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, res)
	return res
}

func deleteAward(t *testing.T, id, applicantId int64) {
	err := dbManager.Award().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateAward(t *testing.T) {
	res := createAward(t)
	deleteAward(t, res.ID, res.ApplicantId)
}

func TestGetAward(t *testing.T) {
	res := createAward(t)
	res2, err := dbManager.Award().Get(res.ID)
	require.NoError(t, err)
	require.NotEmpty(t, res2)

	require.Equal(t, res.ID, res2.ID)
	require.Equal(t, res.ApplicantId, res2.ApplicantId)
	require.Equal(t, res.AwardName, res2.AwardName)
	require.Equal(t, res.Organization, res2.Organization)
	require.Equal(t, res.PdfUrl, res2.PdfUrl)

	deleteAward(t, res.ID, res.ApplicantId)
}

func TestUpdateAward(t *testing.T) {
	res := createAward(t)
	arg := repo.Award{
		ID:           res.ID,
		ApplicantId:  res.ApplicantId,
		AwardName:    res.AwardName,
		Organization: faker.Sentence(),
	}

	res2, err := dbManager.Award().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, arg.ID, res2.ID)
	require.Equal(t, arg.ApplicantId, res2.ApplicantId)
	require.Equal(t, arg.AwardName, res2.AwardName)
	require.Equal(t, arg.Organization, res2.Organization)
	require.Empty(t, res2.PdfUrl)

	deleteAward(t, res.ID, res.ApplicantId)
}

func TestDeleteAward(t *testing.T) {
	edu := createAward(t)
	deleteAward(t, edu.ID, edu.ApplicantId)
}

func TestGetAllAward(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		w := createAward(t)
		ids = append(ids, w.ID)
		apids = append(apids, w.ApplicantId)
	}
	awards, err := dbManager.Award().GetAll(&repo.GetAllAwardParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(awards.Awards), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteAward(t, ids[i], apids[i])
	}
}
