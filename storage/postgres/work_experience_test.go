package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createWorkExp(t *testing.T) *repo.Work {
	applicant := createApplicant(t)
	work, err := dbManager.Work().Create(&repo.Work{
		ApplicantId:    applicant.ID,
		CompanyName:    faker.Word(),
		WorkPosition:   faker.Word(),
		EmploymentType: "full-time",
		StartYear:      faker.YearString(),
		StartMonth:     faker.MonthName(),
		Description:    faker.Sentence(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, work)
	return work
}

func deleteWorkExp(t *testing.T, id, applicantId int64) {
	err := dbManager.Work().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateWorkExp(t *testing.T) {
	work := createWorkExp(t)
	deleteWorkExp(t, work.ID, work.ApplicantId)
}

func TestGetWorkExp(t *testing.T) {
	work := createWorkExp(t)
	w, err := dbManager.Work().Get(work.ID)
	require.NoError(t, err)
	require.NotEmpty(t, w)

	require.Equal(t, w.ID, w.ID)
	require.Equal(t, work.ApplicantId, w.ApplicantId)
	require.Equal(t, work.CompanyName, w.CompanyName)
	require.Equal(t, work.WorkPosition, w.WorkPosition)
	require.Empty(t, w.CompanyWebsite)
	require.Equal(t, work.EmploymentType, w.EmploymentType)
	require.Equal(t, work.StartYear, w.StartYear)
	require.Equal(t, work.StartMonth, w.StartMonth)
	require.Empty(t, w.EndYear)
	require.Empty(t, w.EndMonth)
	require.Equal(t, work.Description, w.Description)

	deleteWorkExp(t, w.ID, w.ApplicantId)
}

func TestUpdateWorkExp(t *testing.T) {
	w1 := createWorkExp(t)
	arg := repo.Work{
		ID:             w1.ID,
		ApplicantId:    w1.ApplicantId,
		EmploymentType: "freelance",
		EndYear:        faker.YearString(),
		EndMonth:       faker.MonthName(),
		Description:    faker.Sentence(),
	}

	w2, err := dbManager.Work().Update(&arg)
	require.NoError(t, err)
	require.Equal(t, w1.ID, w2.ID)
	require.Equal(t, w1.ApplicantId, w2.ApplicantId)
	require.NotEmpty(t, w2.EndYear)
	require.NotEmpty(t, w2.EndMonth)
	require.NotEmpty(t, w2.Description)

	deleteWorkExp(t, w1.ID, w1.ApplicantId)
}

func TestDeleteWorkExp(t *testing.T) {
	w := createWorkExp(t)
	deleteWorkExp(t, w.ID, w.ApplicantId)
}

func TestGetAllWorkExp(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		w := createWorkExp(t)
		ids = append(ids, w.ID)
		apids = append(apids, w.ApplicantId)
	}
	workExps, err := dbManager.Work().GetAll(&repo.GetAllWorkExpParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(workExps.WorkExps), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteWorkExp(t, ids[i], apids[i])
	}
}
