package postgres_test

import (
	"context"
	"database/sql"
	"log"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

const (
	GeneralInfo        = "general_info"
	Work               = "work"
	Education          = "education"
	AcademicTest       = "academic_tests"
	LicenceCertificate = "licences_certificates"
	Award              = "awards"
	Research           = "research"
	Completed          = "completed"
)

func createApplicant(t *testing.T) *repo.Applicant {
	id := utils.RandomInt()
	applicant, err := dbManager.Applicant().Create(context.Background(), &repo.Applicant{
		ID:           id,
		FirstName:    faker.FirstName(),
		LastName:     faker.LastName(),
		Speciality:   faker.Sentence(),
		Country:      faker.Sentence(),
		City:         faker.Sentence(),
		DateOfBirth:  faker.Date(),
		InVisibleAge: true,
		PhoneNumber:  faker.Phonenumber(),
		Email:        faker.Email(),
		About:        faker.Sentence(),
		ImageUrl:     faker.URL(),
		LastStep:     "general_info",
	})
	require.NoError(t, err)
	require.NotEmpty(t, applicant)
	return applicant
}

func deleteApplicant(t *testing.T, id int64) {
	err := dbManager.Applicant().Delete(id)
	require.NoError(t, err)
}

func TestCreateApplicant(t *testing.T) {
	applicant1 := repo.Applicant{
		FirstName:    faker.FirstName(),
		LastName:     faker.LastName(),
		Speciality:   faker.Sentence(),
		Country:      faker.Sentence(),
		City:         faker.Sentence(),
		DateOfBirth:  faker.Date(),
		InVisibleAge: false,
		PhoneNumber:  faker.Phonenumber(),
		Email:        faker.Email(),
		About:        faker.Sentence(),
		ImageUrl:     faker.URL(),
		LastStep:     "general_info",
	}
	applicant2, err := dbManager.Applicant().Create(context.Background(), &applicant1)
	require.NoError(t, err)
	require.Equal(t, applicant1.ID, applicant2.ID)
	require.Equal(t, applicant1.FirstName, applicant2.FirstName)
	require.Equal(t, applicant1.LastName, applicant2.LastName)
	require.Equal(t, applicant1.Speciality, applicant2.Speciality)
	require.Equal(t, applicant1.Country, applicant2.Country)
	require.Equal(t, applicant1.City, applicant2.City)
	require.Equal(t, applicant1.DateOfBirth, applicant2.DateOfBirth)
	require.Equal(t, applicant1.InVisibleAge, applicant2.InVisibleAge)
	require.Equal(t, applicant1.PhoneNumber, applicant2.PhoneNumber)
	require.Equal(t, applicant1.Email, applicant2.Email)
	require.Equal(t, applicant1.About, applicant2.About)
	require.Equal(t, applicant1.ImageUrl, applicant2.ImageUrl)
	require.NotZero(t, applicant2.CreatedAt)
	deleteApplicant(t, applicant2.ID)
}

func TestGetPost(t *testing.T) {
	applicant1 := createApplicant(t)
	a, err := dbManager.Applicant().Get(applicant1.ID)
	require.NoError(t, err)

	require.Equal(t, applicant1.ID, a.ID)
	require.Equal(t, applicant1.FirstName, a.FirstName)
	require.Equal(t, applicant1.LastName, a.LastName)
	require.Equal(t, applicant1.Speciality, a.Speciality)
	require.Equal(t, applicant1.Country, a.Country)
	require.Equal(t, applicant1.City, a.City)
	require.Equal(t, applicant1.DateOfBirth, a.DateOfBirth)
	require.Equal(t, applicant1.InVisibleAge, a.InVisibleAge)
	require.Equal(t, applicant1.PhoneNumber, a.PhoneNumber)
	require.Equal(t, applicant1.Email, a.Email)
	require.Equal(t, applicant1.About, a.About)
	require.Equal(t, applicant1.ImageUrl, a.ImageUrl)
	deleteApplicant(t, applicant1.ID)
}

func TestUpdateApplicant(t *testing.T) {
	applicant1 := createApplicant(t)
	a := repo.Applicant{
		ID:           applicant1.ID,
		FirstName:    faker.FirstName(),
		LastName:     faker.LastName(),
		Speciality:   faker.Sentence(),
		Country:      faker.Sentence(),
		City:         faker.Sentence(),
		DateOfBirth:  faker.Date(),
		InVisibleAge: false,
		PhoneNumber:  faker.Phonenumber(),
		Email:        faker.Email(),
		About:        faker.Sentence(),
		ImageUrl:     faker.URL(),
	}
	applicant2, err := dbManager.Applicant().Update(&a)
	require.NoError(t, err)
	require.Equal(t, a.ID, applicant2.ID)
	require.Equal(t, a.FirstName, applicant2.FirstName)
	require.Equal(t, a.LastName, applicant2.LastName)
	require.Equal(t, a.Speciality, applicant2.Speciality)
	require.Equal(t, a.Country, applicant2.Country)
	require.Equal(t, a.City, applicant2.City)
	require.Equal(t, a.DateOfBirth, applicant2.DateOfBirth)
	require.Equal(t, a.InVisibleAge, applicant2.InVisibleAge)
	require.Equal(t, a.PhoneNumber, applicant2.PhoneNumber)
	require.Equal(t, a.Email, applicant2.Email)
	require.Equal(t, a.About, applicant2.About)
	require.Equal(t, a.ImageUrl, applicant2.ImageUrl)
	require.NotZero(t, applicant2.UpdatedAt)
	deleteApplicant(t, applicant1.ID)
}

func TestUpdateField(t *testing.T) {
	applicant := createApplicant(t)
	applicant2, err := dbManager.Applicant().UpdateStepField(Completed, applicant.Email)
	require.NoError(t, err)
	require.Equal(t, Completed, applicant2.LastStep)
	deleteApplicant(t, applicant.ID)
}

func TestDeleteApplicant(t *testing.T) {
	applicant1 := createApplicant(t)
	deleteApplicant(t, applicant1.ID)
	_, err := dbManager.Applicant().Get(applicant1.ID)
	require.ErrorIs(t, err, sql.ErrNoRows)
}

func TestGetAllApplicantss(t *testing.T) {
	ids := []int64{}
	for i := 0; i < 10; i++ {
		applicant := createApplicant(t)
		ids = append(ids, applicant.ID)
	}
	applicants, err := dbManager.Applicant().GetAll(&repo.GetAllApplicantParams{
		Limit: 10,
		Page:  1,
	})
	log.Println(ids)
	require.GreaterOrEqual(t, len(applicants.Applicants), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteApplicant(t, ids[i])
	}
}
