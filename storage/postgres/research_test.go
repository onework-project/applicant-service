package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createResearch(t *testing.T) *repo.Research {
	applicant := createApplicant(t)
	res, err := dbManager.Research().Create(&repo.Research{
		ApplicantID:      applicant.ID,
		OrganizationName: faker.Word(),
		Position:         faker.Word(),
		Description:      faker.Sentence(),
		Supervisor:       faker.Name(),
		SupervisorEmail:  faker.Email(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, res)
	return res
}

func deleteRes(t *testing.T, id, applicantId int64) {
	err := dbManager.Research().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateResearch(t *testing.T) {
	res := createResearch(t)
	deleteRes(t, res.ID, res.ApplicantID)
}

func TestGetResearch(t *testing.T) {
	res := createResearch(t)
	res2, err := dbManager.Research().Get(res.ID)
	require.NoError(t, err)
	require.NotEmpty(t, res2)

	require.Equal(t, res.ID, res2.ID)
	require.Equal(t, res.ApplicantID, res2.ApplicantID)
	require.Equal(t, res.OrganizationName, res2.OrganizationName)
	require.Equal(t, res.Position, res2.Position)
	require.Equal(t, res.Description, res2.Description)
	require.Equal(t, res.Supervisor, res2.Supervisor)
	require.Equal(t, res.SupervisorEmail, res2.SupervisorEmail)

	deleteRes(t, res.ID, res.ApplicantID)
}

func TestUpdateResearch(t *testing.T) {
	res := createResearch(t)
	arg := repo.Research{
		ID:               res.ID,
		ApplicantID:      res.ApplicantID,
		OrganizationName: faker.Sentence(),
		Position:         faker.Word(),
		Description:      faker.Sentence(),
		Supervisor:       faker.Name(),
	}

	res2, err := dbManager.Research().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, arg.ID, res2.ID)
	require.Equal(t, arg.ApplicantID, res2.ApplicantID)
	require.Equal(t, arg.OrganizationName, res2.OrganizationName)
	require.Equal(t, arg.Position, res2.Position)
	require.Equal(t, arg.Description, res2.Description)
	require.Equal(t, arg.Supervisor, res2.Supervisor)
	require.Empty(t, res2.SupervisorEmail)

	deleteRes(t, res.ID, res.ApplicantID)
}

func TestDeleteResearch(t *testing.T) {
	edu := createResearch(t)
	deleteRes(t, edu.ID, edu.ApplicantID)
}

func TestGetAllResearch(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		w := createResearch(t)
		ids = append(ids, w.ID)
		apids = append(apids, w.ApplicantID)
	}
	researches, err := dbManager.Research().GetAll(&repo.GetAllResearchParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(researches.Researches), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteRes(t, ids[i], apids[i])
	}
}
