package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type socialMediaRepo struct {
	db *sqlx.DB
}

func NewSocialMedia(db *sqlx.DB) repo.SocialMediaStorageI {
	return &socialMediaRepo{
		db: db,
	}
}

func (sr *socialMediaRepo) Create(s *repo.SocialMedia) (*repo.SocialMedia, error) {
	query := `
		INSERT INTO social_medias (
			applicant_id,
			name,
			url
		) VALUES ($1, $2, $3)
		RETURNING id 
	`

	err := sr.db.QueryRow(
		query,
		s.ApplicantId,
		s.Name,
		s.Url,
	).Scan(
		&s.ID,
	)

	if err != nil {
		return nil, err
	}

	return s, nil
}

func (sr *socialMediaRepo) Delete(applicantId int64) error {
	query := `
		DELETE FROM social_medias WHERE applicant_id = $1
	`

	result, err := sr.db.Exec(
		query,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (sr *socialMediaRepo) GetAll(params *repo.GetAllSocialMediasParams) (*repo.GetAllSocialMedias, error) {
	result := repo.GetAllSocialMedias{
		SocialMedias: make([]*repo.SocialMedia, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			name,
			url
		FROM social_medias 
	` + filter + limit

	rows, err := sr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var media repo.SocialMedia
		err := rows.Scan(
			&media.ID,
			&media.ApplicantId,
			&media.Name,
			&media.Url,
		)
		if err != nil {
			return nil, err
		}

		result.SocialMedias = append(result.SocialMedias, &media)
	}

	queryCount := "SELECT count(1) FROM social_medias " + filter

	err = sr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
