package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type awardRepo struct {
	db *sqlx.DB
}

func NewAward(db *sqlx.DB) repo.AwardStorageI {
	return &awardRepo{
		db: db,
	}
}

func (ar *awardRepo) Create(a *repo.Award) (*repo.Award, error) {
	query := `
		INSERT INTO awards (
			applicant_id,
			award_name,
			organization,
			pdf_url
		) VALUES ($1, $2, $3,$4)
		RETURNING id 
	`

	err := ar.db.QueryRow(
		query,
		a.ApplicantId,
		a.AwardName,
		a.Organization,
		utils.NullString(a.PdfUrl),
	).Scan(
		&a.ID,
	)
	if err != nil {
		return nil, err
	}

	return a, nil
}

func (ar *awardRepo) Get(id int64) (*repo.Award, error) {
	var (
		award  repo.Award
		pdfUrl sql.NullString
	)
	query := `
		SELECT
			id,
			applicant_id,
			award_name,
			organization,
			pdf_url
		FROM awards 
		WHERE id = $1
	`
	err := ar.db.QueryRow(
		query,
		id,
	).Scan(
		&award.ID,
		&award.ApplicantId,
		&award.AwardName,
		&award.Organization,
		&pdfUrl,
	)
	if err != nil {
		return nil, err
	}
	award.PdfUrl = pdfUrl.String

	return &award, nil
}

func (er *awardRepo) Update(a *repo.Award) (*repo.Award, error) {
	query := `
		UPDATE awards SET
			award_name = $1,
			organization = $2,
			pdf_url = $3
	    WHERE id = $4 AND applicant_id = $5	
	`

	res, err := er.db.Exec(
		query,
		a.AwardName,
		a.Organization,
		utils.NullString(a.PdfUrl),
		a.ID,
		a.ApplicantId,
	)
	if err != nil {
		return nil, err
	}
	if result, _ := res.RowsAffected(); result == 0 {
		return nil, sql.ErrNoRows
	}

	return a, nil
}

func (ar *awardRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM awards WHERE id = $1 and applicant_id = $2
	`

	result, err := ar.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ar *awardRepo) GetAll(params *repo.GetAllAwardParams) (*repo.GetAllAwards, error) {
	result := repo.GetAllAwards{
		Awards: make([]*repo.Award, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			award_name,
			organization,
			pdf_url
		FROM awards 
	` + filter + limit

	rows, err := ar.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var pdfUrl sql.NullString
	for rows.Next() {
		var award repo.Award
		err := rows.Scan(
			&award.ID,
			&award.ApplicantId,
			&award.AwardName,
			&award.Organization,
			&pdfUrl,
		)
		if err != nil {
			return nil, err
		}
		award.PdfUrl = pdfUrl.String

		result.Awards = append(result.Awards, &award)
	}

	queryCount := "SELECT count(1) FROM awards " + filter

	err = ar.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
