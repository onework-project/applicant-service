package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type blockedCompanyRepo struct {
	db *sqlx.DB
}

func NewBlockedCompany(db *sqlx.DB) repo.BlockedCompanyStorageI {
	return &blockedCompanyRepo{
		db: db,
	}
}

func (sr *blockedCompanyRepo) Create(s *repo.BlockedCompany) (*repo.BlockedCompany, error) {
	query := `
		INSERT INTO blocked_companies (
			applicant_id,
			company_id
		) VALUES ($1, $2)
		RETURNING id, created_at
	`

	err := sr.db.QueryRow(
		query,
		s.ApplicantID,
		s.CompanyID,
	).Scan(
		&s.ID,
		&s.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (lr *blockedCompanyRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM blocked_companies WHERE id = $1 and applicant_id = $2
	`

	result, err := lr.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *blockedCompanyRepo) GetAll(params *repo.GetAllBlockedCompaniesParams) (*repo.GetAllBlockedCompanies, error) {
	result := repo.GetAllBlockedCompanies{
		BlockedCompanies: make([]*repo.BlockedCompany, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			company_id,
			created_at
		FROM blocked_companies 
	` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var skill repo.BlockedCompany
		err := rows.Scan(
			&skill.ID,
			&skill.ApplicantID,
			&skill.CompanyID,
			&skill.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.BlockedCompanies = append(result.BlockedCompanies, &skill)
	}

	queryCount := "SELECT count(1) FROM blocked_companies " + filter

	err = lr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
