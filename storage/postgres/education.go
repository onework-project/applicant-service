package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type educationRepo struct {
	db *sqlx.DB
}

func NewEducation(db *sqlx.DB) repo.EducationStorageI {
	return &educationRepo{
		db: db,
	}
}

func (er *educationRepo) Create(edu *repo.Education) (*repo.Education, error) {
	query := `
		INSERT INTO educations (
			applicant_id,
			university,
			degree_name,
			degree_level,
			start_year,
			end_year,
			applicant_grade,
			max_grade,
			pdf_url
		) VALUES (
			$1,$2,$3,
			$4,$5,$6,
			$7,$8,$9
		)
		RETURNING id 
	`

	err := er.db.QueryRow(
		query,
		edu.ApplicantID,
		edu.University,
		edu.DegreeName,
		edu.DegreeLevel,
		edu.StartYear,
		edu.EndYear,
		utils.NullInt64(edu.ApplicantGrade),
		utils.NullInt64(edu.MaxGrade),
		utils.NullString(edu.PdfUrl),
	).Scan(&edu.ID)
	if err != nil {
		return nil, err
	}

	return edu, nil
}

func (er *educationRepo) Get(id int64) (*repo.Education, error) {
	var (
		res                      repo.Education
		applicantGrade, maxGrade sql.NullInt64
		pdfUrl                   sql.NullString
	)

	query := `
		SELECT 
			id,
			applicant_id,
			university,
			degree_name,
			degree_level,
			start_year,
			end_year,
			applicant_grade,
			max_grade,
			pdf_url
		FROM educations 
		WHERE id = $1
	`

	err := er.db.QueryRow(
		query,
		id,
	).Scan(
		&res.ID,
		&res.ApplicantID,
		&res.University,
		&res.DegreeName,
		&res.DegreeLevel,
		&res.StartYear,
		&res.EndYear,
		&applicantGrade,
		&maxGrade,
		&pdfUrl,
	)
	if err != nil {
		return nil, err
	}
	res.ApplicantGrade = applicantGrade.Int64
	res.MaxGrade = maxGrade.Int64
	res.PdfUrl = pdfUrl.String

	return &res, nil
}

func (er *educationRepo) Update(edu *repo.Education) (*repo.Education, error) {
	query := `
		UPDATE educations SET
			university = $1,
			degree_name = $2,
			degree_level = $3,
			start_year = $4,
			end_year = $5,
			applicant_grade = $6,
			max_grade = $7,
			pdf_url = $8
	    WHERE id = $9 AND applicant_id = $10	
	`

	res, err := er.db.Exec(
		query,
		edu.University,
		edu.DegreeName,
		edu.DegreeLevel,
		edu.StartYear,
		edu.EndYear,
		utils.NullInt64(edu.ApplicantGrade),
		utils.NullInt64(edu.MaxGrade),
		utils.NullString(edu.PdfUrl),
		edu.ID,
		edu.ApplicantID,
	)
	if err != nil {
		return nil, err
	}
	if result, _ := res.RowsAffected(); result == 0 {
		return nil, sql.ErrNoRows
	}

	return edu, nil
}

func (er *educationRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM educations WHERE id = $1 and applicant_id = $2
	`

	result, err := er.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (er *educationRepo) GetAll(params *repo.GetAllEduParams) (*repo.GetAllEdus, error) {
	result := repo.GetAllEdus{
		Educations: make([]*repo.Education, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			university,
			degree_name,
			degree_level,
			start_year,
			end_year,
			applicant_grade,
			max_grade,
			pdf_url
		FROM educations 
	` + filter + limit

	rows, err := er.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var (
		applicantGrade, maxGrade sql.NullInt64
		pdfUrl                   sql.NullString
	)
	for rows.Next() {
		var edu repo.Education
		err := rows.Scan(
			&edu.ID,
			&edu.ApplicantID,
			&edu.University,
			&edu.DegreeName,
			&edu.DegreeLevel,
			&edu.StartYear,
			&edu.EndYear,
			&applicantGrade,
			&maxGrade,
			&pdfUrl,
		)
		if err != nil {
			return nil, err
		}
		edu.ApplicantGrade = applicantGrade.Int64
		edu.MaxGrade = maxGrade.Int64
		edu.PdfUrl = pdfUrl.String

		result.Educations = append(result.Educations, &edu)
	}

	queryCount := "SELECT count(1) FROM educations " + filter

	err = er.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
