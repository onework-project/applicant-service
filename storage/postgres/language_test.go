package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createLanguage(t *testing.T) *repo.Language {
	applicant := createApplicant(t)
	l, err := dbManager.Language().Create(&repo.Language{
		ApplicantId: applicant.ID,
		Language:    faker.Word(),
		Level:       "N",
	})
	require.NoError(t, err)
	require.NotEmpty(t, l)
	return l
}

func deleteLanguage(t *testing.T, applicantId int64) {
	err := dbManager.Language().Delete(applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateLanguage(t *testing.T) {
	l := createLanguage(t)
	deleteLanguage(t, l.ApplicantId)
}

func TestDeleteLanguage(t *testing.T) {
	s1 := createLanguage(t)
	deleteLanguage(t, s1.ApplicantId)
}

func TestGetAllLanguage(t *testing.T) {
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createLanguage(t)
		apids = append(apids, s1.ApplicantId)
	}
	smedias, err := dbManager.Language().GetAll(&repo.GetAllLanguageParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(smedias.Languages), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteLanguage(t, apids[i])
	}
}
