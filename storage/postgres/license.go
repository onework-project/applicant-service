package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type licenseRepo struct {
	db *sqlx.DB
}

func NewLicense(db *sqlx.DB) repo.LicenseStorageI {
	return &licenseRepo{
		db: db,
	}
}

func (lr *licenseRepo) Create(l *repo.License) (*repo.License, error) {
	query := `
		INSERT INTO licenses_and_sertificates (
			applicant_id,
			name,
			organization,
			pdf_url
		) VALUES ($1, $2, $3, $4)
		RETURNING id 
	`

	err := lr.db.QueryRow(
		query,
		l.ApplicantId,
		l.Name,
		l.Organization,
		utils.NullString(l.PdfUrl),
	).Scan(
		&l.ID,
	)
	if err != nil {
		return nil, err
	}

	return l, nil
}

func (lr *licenseRepo) Get(id int64) (*repo.License, error) {
	var (
		license repo.License
		pdfUrl  sql.NullString
	)
	query := `
		SELECT
			id,
			applicant_id,
			name,
			organization,
			pdf_url
		FROM licenses_and_sertificates 
		WHERE id = $1
	`
	err := lr.db.QueryRow(
		query,
		id,
	).Scan(
		&license.ID,
		&license.ApplicantId,
		&license.Name,
		&license.Organization,
		&pdfUrl,
	)
	if err != nil {
		return nil, err
	}
	license.PdfUrl = pdfUrl.String

	return &license, nil
}

func (lr *licenseRepo) Update(licence *repo.License) (*repo.License, error) {
	query := `
		UPDATE licenses_and_sertificates SET
			name = $1,
			organization = $2,
			pdf_url = $3
		WHERE id = $4 and applicant_id = $5
	`
	result, err := lr.db.Exec(
		query,
		licence.Name,
		licence.Organization,
		utils.NullString(licence.PdfUrl),
		licence.ID,
		licence.ApplicantId,
	)
	if err != nil {
		return nil, err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return nil, sql.ErrNoRows
	}

	return licence, nil
}

func (lr *licenseRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM licenses_and_sertificates WHERE id = $1 and applicant_id = $2
	`

	result, err := lr.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *licenseRepo) GetAll(params *repo.GetAllLicenseParams) (*repo.GetAllLicenses, error) {
	result := repo.GetAllLicenses{
		Licenses: make([]*repo.License, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			name,
			organization,
			pdf_url
		FROM licenses_and_sertificates 
	` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var pdfUrl sql.NullString
	for rows.Next() {
		var license repo.License
		err := rows.Scan(
			&license.ID,
			&license.ApplicantId,
			&license.Name,
			&license.Organization,
			&pdfUrl,
		)
		if err != nil {
			return nil, err
		}
		license.PdfUrl = pdfUrl.String

		result.Licenses = append(result.Licenses, &license)
	}

	queryCount := "SELECT count(1) FROM licenses_and_sertificates " + filter

	err = lr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
