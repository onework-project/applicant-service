package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type savedJobRepo struct {
	db *sqlx.DB
}

func NewSavedJob(db *sqlx.DB) repo.SavedJobStorageI {
	return &savedJobRepo{
		db: db,
	}
}

func (sr *savedJobRepo) Create(s *repo.SavedJob) (*repo.SavedJob, error) {
	query := `
		INSERT INTO saved_jobs (
			applicant_id,
			vacancy_id
		) VALUES ($1, $2)
		RETURNING id, created_at
	`

	err := sr.db.QueryRow(
		query,
		s.ApplicantID,
		s.VacancyID,
	).Scan(
		&s.ID,
		&s.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (lr *savedJobRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM saved_jobs WHERE id = $1 and applicant_id = $2
	`

	result, err := lr.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *savedJobRepo) GetAll(params *repo.GetAllSavedJobsParams) (*repo.GetAllSavedJobs, error) {
	result := repo.GetAllSavedJobs{
		SavedJobs: make([]*repo.SavedJob, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			vacancy_id,
			created_at
		FROM saved_jobs 
	` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var skill repo.SavedJob
		err := rows.Scan(
			&skill.ID,
			&skill.ApplicantID,
			&skill.VacancyID,
			&skill.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.SavedJobs = append(result.SavedJobs, &skill)
	}

	queryCount := "SELECT count(1) FROM saved_jobs " + filter

	err = lr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
