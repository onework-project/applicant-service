package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createSocialMedia(t *testing.T) *repo.SocialMedia {
	applicant := createApplicant(t)
	media, err := dbManager.SocialMedia().Create(&repo.SocialMedia{
		ApplicantId: applicant.ID,
		Name:        "website",
		Url:         faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, media)
	return media
}

func deleteSocialMedia(t *testing.T, applicantId int64) {
	err := dbManager.SocialMedia().Delete(applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateSocialMedia(t *testing.T) {
	socialMedia := createSocialMedia(t)
	deleteSocialMedia(t, socialMedia.ApplicantId)
}

func TestDeleteSocialMedia(t *testing.T) {
	s1 := createSocialMedia(t)
	deleteSocialMedia(t, s1.ApplicantId)
}

func TestGetAllSocialMedias(t *testing.T) {
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createSocialMedia(t)
		apids = append(apids, s1.ApplicantId)
	}
	smedias, err := dbManager.SocialMedia().GetAll(&repo.GetAllSocialMediasParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(smedias.SocialMedias), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteSocialMedia(t, apids[i])
	}
}
