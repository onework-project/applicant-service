package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createAcademic(t *testing.T) *repo.Academic {
	applicant := createApplicant(t)
	res, err := dbManager.Academic().Create(&repo.Academic{
		ApplicantID:  applicant.ID,
		TestName:     faker.Word(),
		Organization: faker.Word(),
		Score:        8,
		PdfUrl:       faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, res)
	return res
}

func deleteAcademic(t *testing.T, id, applicantId int64) {
	err := dbManager.Academic().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateAcademic(t *testing.T) {
	res := createAcademic(t)
	deleteAcademic(t, res.ID, res.ApplicantID)
}

func TestGetAcademic(t *testing.T) {
	res := createAcademic(t)
	res2, err := dbManager.Academic().Get(res.ID)
	require.NoError(t, err)
	require.NotEmpty(t, res2)

	require.Equal(t, res.ID, res2.ID)
	require.Equal(t, res.ApplicantID, res2.ApplicantID)
	require.Equal(t, res.TestName, res2.TestName)
	require.Equal(t, res.Organization, res2.Organization)
	require.Equal(t, res.Score, res2.Score)
	require.Equal(t, res.PdfUrl, res2.PdfUrl)

	deleteAcademic(t, res.ID, res.ApplicantID)
}

func TestUpdateAcademic(t *testing.T) {
	res := createAcademic(t)
	arg := repo.Academic{
		ID:           res.ID,
		ApplicantID:  res.ApplicantID,
		TestName:     res.TestName,
		Organization: faker.Sentence(),
		Score:        4,
		PdfUrl:       faker.URL(),
	}

	res2, err := dbManager.Academic().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, arg.ID, res2.ID)
	require.Equal(t, arg.ApplicantID, res2.ApplicantID)
	require.Equal(t, arg.TestName, res2.TestName)
	require.Equal(t, arg.Organization, res2.Organization)
	require.Equal(t, arg.Score, res2.Score)
	require.Equal(t, arg.PdfUrl, res2.PdfUrl)

	deleteAcademic(t, res.ID, res.ApplicantID)
}

func TestDeleteAcademic(t *testing.T) {
	edu := createAcademic(t)
	deleteAcademic(t, edu.ID, edu.ApplicantID)
}

func TestGetAllAcademics(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		w := createAcademic(t)
		ids = append(ids, w.ID)
		apids = append(apids, w.ApplicantID)
	}
	academics, err := dbManager.Academic().GetAll(&repo.GetAllAcademicsParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(academics.Academics), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteAcademic(t, ids[i], apids[i])
	}
}
