package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createBlockedCompany(t *testing.T) *repo.BlockedCompany {
	applicant := createApplicant(t)
	blockedCompany, err := dbManager.BlockedCompany().Create(&repo.BlockedCompany{
		ApplicantID: applicant.ID,
		CompanyID:   2,
	})
	require.NoError(t, err)
	require.NotEmpty(t, blockedCompany)
	return blockedCompany
}

func deleteBlockedCompany(t *testing.T, id, applicantId int64) {
	err := dbManager.BlockedCompany().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateBlockedCompany(t *testing.T) {
	blockedCompany := createBlockedCompany(t)
	deleteBlockedCompany(t, blockedCompany.ID, blockedCompany.ApplicantID)
}

func TestDeleteBlockedCompany(t *testing.T) {
	s1 := createBlockedCompany(t)
	deleteBlockedCompany(t, s1.ID, s1.ApplicantID)
}

func TestGetAllBlockedCompany(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createBlockedCompany(t)
		ids = append(ids, s1.ID)
		apids = append(apids, s1.ApplicantID)
	}
	skills, err := dbManager.BlockedCompany().GetAll(&repo.GetAllBlockedCompaniesParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(skills.BlockedCompanies), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteBlockedCompany(t, ids[i], apids[i])
	}
}
