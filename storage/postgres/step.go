package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type stepRepo struct {
	db *sqlx.DB
}

func NewStep(db *sqlx.DB) repo.StepStorageI {
	return &stepRepo{
		db: db,
	}
}

func (sr *stepRepo) Create(applicant_id int64) (*repo.Step, error) {
	var s repo.Step
	query := `
		INSERT INTO steps (
			applicant_id
		) VALUES ($1) 
		RETURNING id, applicant_id, general_info, 
		work, education, academic_tests, 
		licences_certificates, awards, research
	`
	err := sr.db.QueryRow(
		query,
		applicant_id,
	).Scan(
		&s.ID,
		&s.ApplicantID,
		&s.GeneralInfo,
		&s.Work,
		&s.Education,
		&s.AcademicTests,
		&s.LicenceCertificates,
		&s.Award,
		&s.Research,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *stepRepo) Get(applicant_id int64) (*repo.Step, error) {
	var s repo.Step
	query := `
		SELECT
			id,
			applicant_id,
			general_info,
			work,
			education,
			academic_tests,
			licences_certificates,
			awards,
			research
		FROM steps WHERE applicant_id = $1
	`
	err := sr.db.QueryRow(
		query,
		applicant_id,
	).Scan(
		&s.ID,
		&s.ApplicantID,
		&s.GeneralInfo,
		&s.Work,
		&s.Education,
		&s.AcademicTests,
		&s.LicenceCertificates,
		&s.Award,
		&s.Research,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *stepRepo) UpdateField(field string, arg bool, applicant_id int64) (*repo.Step, error) {
	var s repo.Step
	query := fmt.Sprintf(`
		UPDATE steps SET 
		%s = $1
		WHERE applicant_id = $2
		RETURNING id, applicant_id, general_info, 
		work, education, academic_tests, 
		licences_certificates, awards, research
	`, field)
	err := sr.db.QueryRow(
		query,
		arg,
		applicant_id,
	).Scan(
		&s.ID,
		&s.ApplicantID,
		&s.GeneralInfo,
		&s.Work,
		&s.Education,
		&s.AcademicTests,
		&s.LicenceCertificates,
		&s.Award,
		&s.Research,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *stepRepo) Delete(applicant_id int64) error {
	query := `
		DELETE FROM steps WHERE applicant_id = $1
	`
	res, err := sr.db.Exec(query, applicant_id)
	if err != nil {
		return err
	}
	if r, _ := res.RowsAffected(); r == 0 {
		return sql.ErrNoRows
	}

	return nil
}
