package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createEdu(t *testing.T) *repo.Education {
	applicant := createApplicant(t)
	edu, err := dbManager.Education().Create(&repo.Education{
		ApplicantID: applicant.ID,
		University:  faker.Sentence(),
		DegreeName:  faker.Word(),
		DegreeLevel: faker.Word(),
		StartYear:   faker.YearString(),
		EndYear:     faker.YearString(),
		PdfUrl:      faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, edu)
	return edu
}

func deleteEdu(t *testing.T, id, applicantId int64) {
	err := dbManager.Education().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateEdu(t *testing.T) {
	edu := createEdu(t)
	deleteEdu(t, edu.ID, edu.ApplicantID)
}

func TestGetEdu(t *testing.T) {
	edu := createEdu(t)
	edu2, err := dbManager.Education().Get(edu.ID)
	require.NoError(t, err)
	require.NotEmpty(t, edu2)

	require.Equal(t, edu.ID, edu.ID)
	require.Equal(t, edu.ApplicantID, edu2.ApplicantID)
	require.Equal(t, edu.University, edu2.University)
	require.Equal(t, edu.DegreeName, edu2.DegreeName)
	require.Equal(t, edu.DegreeLevel, edu2.DegreeLevel)
	require.Equal(t, edu.StartYear, edu2.StartYear)
	require.Equal(t, edu.EndYear, edu2.EndYear)
	require.Empty(t, edu2.ApplicantGrade)
	require.Empty(t, edu2.MaxGrade)
	require.Equal(t, edu.PdfUrl, edu2.PdfUrl)

	deleteEdu(t, edu.ID, edu.ApplicantID)
}

func TestUpdateEdu(t *testing.T) {
	edu1 := createEdu(t)
	arg := repo.Education{
		ID:          edu1.ID,
		ApplicantID: edu1.ApplicantID,
		University:  faker.Word(),
		DegreeName:  faker.Word(),
		DegreeLevel: faker.Word(),
		StartYear:   faker.YearString(),
		EndYear:     faker.YearString(),
	}

	edu2, err := dbManager.Education().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, edu1.ID, edu1.ID)
	require.Equal(t, edu1.ApplicantID, edu2.ApplicantID)
	require.NotEqual(t, edu1.University, edu2.University)
	require.NotEqual(t, edu1.DegreeName, edu2.DegreeName)
	require.NotEqual(t, edu1.DegreeLevel, edu2.DegreeLevel)
	require.NotEqual(t, edu1.StartYear, edu2.StartYear)
	require.NotEqual(t, edu1.EndYear, edu2.EndYear)

	deleteEdu(t, edu1.ID, edu1.ApplicantID)
}

func TestDeleteEdu(t *testing.T) {
	edu := createEdu(t)
	deleteEdu(t, edu.ID, edu.ApplicantID)
}

func TestGetAllEdu(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		w := createEdu(t)
		ids = append(ids, w.ID)
		apids = append(apids, w.ApplicantID)
	}
	edus, err := dbManager.Education().GetAll(&repo.GetAllEduParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(edus.Educations), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteEdu(t, ids[i], apids[i])
	}
}
