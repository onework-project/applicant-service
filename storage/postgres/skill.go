package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type skillRepo struct {
	db *sqlx.DB
}

func NewSkill(db *sqlx.DB) repo.SkillStorageI {
	return &skillRepo{
		db: db,
	}
}

func (sr *skillRepo) Create(s *repo.Skill) (*repo.Skill, error) {
	query := `
		INSERT INTO skills (
			applicant_id,
			name
		) VALUES ($1, $2)
		RETURNING id 
	`

	err := sr.db.QueryRow(
		query,
		s.ApplicantId,
		s.Name,
	).Scan(
		&s.ID,
	)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (lr *skillRepo) Delete(applicantId int64) error {
	query := `
		DELETE FROM skills WHERE applicant_id = $1
	`

	result, err := lr.db.Exec(
		query,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *skillRepo) GetAll(params *repo.GetAllSkillParams) (*repo.GetAllSkills, error) {
	result := repo.GetAllSkills{
		Skills: make([]*repo.Skill, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			name
		FROM skills 
	` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var skill repo.Skill
		err := rows.Scan(
			&skill.ID,
			&skill.ApplicantId,
			&skill.Name,
		)
		if err != nil {
			return nil, err
		}

		result.Skills = append(result.Skills, &skill)
	}

	queryCount := "SELECT count(1) FROM skills " + filter

	err = lr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
