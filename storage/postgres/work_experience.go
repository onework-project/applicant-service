package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type workRepo struct {
	db *sqlx.DB
}

func NewWork(db *sqlx.DB) repo.WorkStorageI {
	return &workRepo{
		db: db,
	}
}

func (wr *workRepo) Create(work *repo.Work) (*repo.Work, error) {
	query := `
		INSERT INTO work_experiences (
			applicant_id,
			company_name,
			work_position,
			company_website,
			employment_type,
			start_year,
			start_month,
			end_year,
			end_month,
			description
		) VALUES (
			$1,$2,$3,
			$4,$5,$6,
			$7,$8,$9, $10
		)
		RETURNING id 
	`

	err := wr.db.QueryRow(
		query,
		work.ApplicantId,
		work.CompanyName,
		work.WorkPosition,
		utils.NullString(work.CompanyWebsite),
		work.EmploymentType,
		work.StartYear,
		work.StartMonth,
		utils.NullString(work.EndYear),
		utils.NullString(work.EndMonth),
		utils.NullString(work.Description),
	).Scan(&work.ID)
	if err != nil {
		return nil, err
	}

	return work, nil
}

func (wr *workRepo) Get(id int64) (*repo.Work, error) {
	var (
		res                                            repo.Work
		companyWebsite, endYear, endMonth, description sql.NullString
	)

	query := `
		SELECT 
			id,
			applicant_id,
			company_name,
			work_position,
			company_website,
			employment_type,
			start_year,
			start_month,
			end_year,
			end_month,
			description
		FROM work_experiences 
		WHERE id = $1
	`

	err := wr.db.QueryRow(
		query,
		id,
	).Scan(
		&res.ID,
		&res.ApplicantId,
		&res.CompanyName,
		&res.WorkPosition,
		&companyWebsite,
		&res.EmploymentType,
		&res.StartYear,
		&res.StartMonth,
		&endYear,
		&endMonth,
		&description,
	)
	if err != nil {
		return nil, err
	}
	res.CompanyWebsite = companyWebsite.String
	res.EndYear = endYear.String
	res.EndMonth = endMonth.String
	res.Description = description.String

	return &res, nil
}

func (wr *workRepo) Update(work *repo.Work) (*repo.Work, error) {
	query := `
		UPDATE work_experiences SET
			company_name = $1,
			work_position = $2,
			company_website = $3,
			employment_type = $4,
			start_year = $5,
			start_month = $6,
			end_year = $7,
			end_month = $8,
			description = $9		
	    WHERE id = $10 AND applicant_id = $11	
	`

	res, err := wr.db.Exec(
		query,
		work.CompanyName,
		work.WorkPosition,
		utils.NullString(work.CompanyWebsite),
		work.EmploymentType,
		work.StartYear,
		work.StartMonth,
		utils.NullString(work.EndYear),
		utils.NullString(work.EndMonth),
		utils.NullString(work.Description),
		work.ID,
		work.ApplicantId,
	)
	if err != nil {
		return nil, err
	}
	if result, _ := res.RowsAffected(); result == 0 {
		return nil, sql.ErrNoRows
	}

	return work, nil
}

func (wr *workRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM work_experiences WHERE id = $1 and applicant_id = $2
	`

	result, err := wr.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (wr *workRepo) GetAll(params *repo.GetAllWorkExpParams) (*repo.GetAllWorkExps, error) {
	result := repo.GetAllWorkExps{
		WorkExps: make([]*repo.Work, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			company_name,
			work_position,
			company_website,
			employment_type,
			start_year,
			start_month,
			end_year,
			end_month,
			description
		FROM work_experiences 
	` + filter + limit

	rows, err := wr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var (
		companyWebsite, endYear, endMonth, description sql.NullString
	)
	for rows.Next() {
		var work repo.Work
		err := rows.Scan(
			&work.ID,
			&work.ApplicantId,
			&work.CompanyName,
			&work.WorkPosition,
			&companyWebsite,
			&work.EmploymentType,
			&work.StartYear,
			&work.StartMonth,
			&endYear,
			&endMonth,
			&description,
		)
		if err != nil {
			return nil, err
		}
		work.CompanyWebsite = companyWebsite.String
		work.EndYear = endYear.String
		work.EndMonth = endMonth.String
		work.Description = description.String

		result.WorkExps = append(result.WorkExps, &work)
	}

	queryCount := "SELECT count(1) FROM work_experiences " + filter

	err = wr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
