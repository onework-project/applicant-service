package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type researchRepo struct {
	db *sqlx.DB
}

func NewResearch(db *sqlx.DB) repo.ResearchStorageI {
	return &researchRepo{
		db: db,
	}
}

func (r *researchRepo) Create(res *repo.Research) (*repo.Research, error) {
	query := `
		INSERT INTO researches (
			applicant_id,
			organization_name,
			position,
			description,
			superviser,
			supervisor_email
		) VALUES (
			$1,$2,$3,
			$4,$5,$6
		)
		RETURNING id 
	`

	err := r.db.QueryRow(
		query,
		res.ApplicantID,
		res.OrganizationName,
		res.Position,
		res.Description,
		res.Supervisor,
		utils.NullString(res.SupervisorEmail),
	).Scan(&res.ID)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *researchRepo) Get(id int64) (*repo.Research, error) {
	var (
		res   repo.Research
		email sql.NullString
	)

	query := `
		SELECT 
			id,
			applicant_id,
			organization_name,
			position,
			description,
			superviser,
			supervisor_email
		FROM researches 
		WHERE id = $1
	`

	err := r.db.QueryRow(
		query,
		id,
	).Scan(
		&res.ID,
		&res.ApplicantID,
		&res.OrganizationName,
		&res.Position,
		&res.Description,
		&res.Supervisor,
		&email,
	)
	if err != nil {
		return nil, err
	}
	res.SupervisorEmail = email.String

	return &res, nil
}

func (r *researchRepo) Update(res *repo.Research) (*repo.Research, error) {
	query := `
		UPDATE researches SET
			organization_name = $1,
			position = $2,
			description = $3,
			superviser = $4,
			supervisor_email = $5
	    WHERE id = $6 AND applicant_id = $7	
	`

	result, err := r.db.Exec(
		query,
		res.OrganizationName,
		res.Position,
		res.Description,
		res.Supervisor,
		utils.NullString(res.Supervisor),
		res.ID,
		res.ApplicantID,
	)
	if err != nil {
		return nil, err
	}
	if r, _ := result.RowsAffected(); r == 0 {
		return nil, sql.ErrNoRows
	}

	return res, nil
}

func (r *researchRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM researches WHERE id = $1 and applicant_id = $2
	`

	result, err := r.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (r *researchRepo) GetAll(params *repo.GetAllResearchParams) (*repo.GetAllResearches, error) {
	result := repo.GetAllResearches{
		Researches: make([]*repo.Research, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			organization_name,
			position,
			description,
			superviser,
			supervisor_email
		FROM researches 
	` + filter + limit

	rows, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var (
		email sql.NullString
	)
	for rows.Next() {
		var res repo.Research
		err := rows.Scan(
			&res.ID,
			&res.ApplicantID,
			&res.OrganizationName,
			&res.Position,
			&res.Description,
			&res.Supervisor,
			&email,
		)
		if err != nil {
			return nil, err
		}
		res.SupervisorEmail = email.String

		result.Researches = append(result.Researches, &res)
	}

	queryCount := "SELECT count(1) FROM educations " + filter

	err = r.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
