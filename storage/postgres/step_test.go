package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func CreateStep(t *testing.T) *repo.Step {
	applicant := createApplicant(t)
	step, err := dbManager.Step().Create(applicant.ID)
	require.NoError(t, err)

	require.Equal(t, applicant.ID, step.ApplicantID)
	require.False(t, step.Work)
	require.False(t, step.Research)
	require.False(t, step.LicenceCertificates)
	require.False(t, step.GeneralInfo)
	require.False(t, step.Education)
	require.False(t, step.Award)
	require.False(t, step.AcademicTests)

	return step
}

func deleteStep(t *testing.T, applicantId int64) {
	err := dbManager.Step().Delete(applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateStep(t *testing.T) {
	step := CreateStep(t)
	deleteStep(t, step.ApplicantID)
}

func TestUpdateStepField(t *testing.T) {
	step := CreateStep(t)
	step2, err := dbManager.Step().UpdateField(Education, true, step.ApplicantID)
	require.NoError(t, err)
	require.True(t, step2.Education)
	require.Equal(t, step.ApplicantID, step2.ApplicantID)
	require.False(t, step2.Work)
	require.False(t, step2.Research)
	require.False(t, step2.LicenceCertificates)
	require.False(t, step2.GeneralInfo)
	require.False(t, step2.Award)
	require.False(t, step2.AcademicTests)
	deleteStep(t, step.ApplicantID)
}
