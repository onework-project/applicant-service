package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createLicense(t *testing.T) *repo.License {
	applicant := createApplicant(t)
	l, err := dbManager.License().Create(&repo.License{
		ApplicantId:  applicant.ID,
		Name:         faker.Name(),
		Organization: faker.Sentence(),
		PdfUrl:       faker.URL(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, l)
	return l
}

func deleteLicense(t *testing.T, id, applicantId int64) {
	err := dbManager.License().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateLicense(t *testing.T) {
	l := createLicense(t)
	deleteLicense(t, l.ID, l.ApplicantId)
}

func TestGetLicense(t *testing.T) {
	l := createLicense(t)
	s, err := dbManager.License().Get(l.ID)
	require.NoError(t, err)

	require.Equal(t, l.ID, s.ID)
	require.Equal(t, l.ApplicantId, s.ApplicantId)
	require.Equal(t, l.Name, s.Name)
	require.Equal(t, l.Organization, s.Organization)
	deleteLicense(t, s.ID, s.ApplicantId)
}

func TestUpdateLicense(t *testing.T) {
	l := createLicense(t)
	arg := repo.License{
		ID:           l.ID,
		ApplicantId:  l.ApplicantId,
		Name:         faker.Word(),
		Organization: faker.Sentence(),
	}
	s, err := dbManager.License().Update(&arg)
	require.NoError(t, err)

	require.Equal(t, l.ID, s.ID)
	require.Equal(t, l.ApplicantId, s.ApplicantId)
	require.NotEqual(t, l.Name, s.Name)
	require.NotEqual(t, l.Organization, s.Organization)
	require.Empty(t, s.PdfUrl)
	deleteLicense(t, s.ID, s.ApplicantId)
}

func TestDeleteLicense(t *testing.T) {
	s1 := createLicense(t)
	deleteLicense(t, s1.ID, s1.ApplicantId)
}

func TestGetAllLicence(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createLicense(t)
		ids = append(ids, s1.ID)
		apids = append(apids, s1.ApplicantId)
	}
	licenses, err := dbManager.License().GetAll(&repo.GetAllLicenseParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(licenses.Licenses), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteLicense(t, ids[i], apids[i])
	}
}
