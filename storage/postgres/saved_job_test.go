package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createSavedJob(t *testing.T) *repo.SavedJob {
	applicant := createApplicant(t)
	savedJob, err := dbManager.SavedJob().Create(&repo.SavedJob{
		ApplicantID: applicant.ID,
		VacancyID:   2,
	})
	require.NoError(t, err)
	require.NotEmpty(t, savedJob)
	return savedJob
}

func deleteSavedJob(t *testing.T, id, applicantId int64) {
	err := dbManager.SavedJob().Delete(id, applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateSavedJob(t *testing.T) {
	savedJob := createSavedJob(t)
	deleteSavedJob(t, savedJob.ID, savedJob.ApplicantID)
}

func TestDeleteSavedJob(t *testing.T) {
	s1 := createSavedJob(t)
	deleteSavedJob(t, s1.ID, s1.ApplicantID)
}

func TestGetAllSavedJob(t *testing.T) {
	ids := []int64{}
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createSavedJob(t)
		ids = append(ids, s1.ID)
		apids = append(apids, s1.ApplicantID)
	}
	skills, err := dbManager.SavedJob().GetAll(&repo.GetAllSavedJobsParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(skills.SavedJobs), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteSavedJob(t, ids[i], apids[i])
	}
}
