package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"go.opentelemetry.io/otel/trace"
)

type applicantRepo struct {
	db     *sqlx.DB
	tracer trace.Tracer
}

func NewApplicant(db *sqlx.DB, tracer trace.Tracer) repo.ApplicantStorageI {
	return &applicantRepo{
		db:     db,
		tracer: tracer,
	}
}

func (ar *applicantRepo) Create(ctx context.Context, applicant *repo.Applicant) (*repo.Applicant, error) {
	_, span := ar.tracer.Start(ctx, "postgres.CreateApplicant")
	defer span.End()
	var (
		updatedAt sql.NullTime
	)
	query := `
		INSERT INTO applicants (
			id,
			first_name,
			last_name,
			speciality,
			country,
			city,
			date_of_birth,
			invisible_age,
			phone_number,
			email,
			about,
			image_url,
			last_step,
			slug
		) VALUES (
			$1,$2,$3,
			$4,$5,$6,
			$7,$8,$9,
			$10, $11, 
			$12, 'general_info', $13
		)
		RETURNING last_step, created_at, updated_at
	`

	err := ar.db.QueryRow(
		query,
		applicant.ID,
		applicant.FirstName,
		applicant.LastName,
		applicant.Speciality,
		applicant.Country,
		applicant.City,
		applicant.DateOfBirth,
		applicant.InVisibleAge,
		applicant.PhoneNumber,
		applicant.Email,
		applicant.About,
		applicant.ImageUrl,
		applicant.Slug,
	).Scan(
		&applicant.LastStep,
		&applicant.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}

	applicant.UpdatedAt = updatedAt.Time

	return applicant, nil
}

func (ar *applicantRepo) Get(id int64) (*repo.Applicant, error) {
	query := `
		SELECT 
			id,
			first_name,
			last_name,
			speciality,
			country,
			city,
			date_of_birth,
			invisible_age,
			phone_number,
			email,
			about,
			image_url,
			last_step,
			created_at,
			updated_at,
			slug
		FROM applicants WHERE id = $1
	`

	var (
		result    repo.Applicant
		updatedAt sql.NullTime
	)

	err := ar.db.QueryRow(
		query,
		id,
	).Scan(
		&result.ID,
		&result.FirstName,
		&result.LastName,
		&result.Speciality,
		&result.Country,
		&result.City,
		&result.DateOfBirth,
		&result.InVisibleAge,
		&result.PhoneNumber,
		&result.Email,
		&result.About,
		&result.ImageUrl,
		&result.LastStep,
		&result.CreatedAt,
		&updatedAt,
		&result.Slug,
	)
	if err != nil {
		return nil, err
	}

	result.UpdatedAt = updatedAt.Time

	return &result, nil
}

func (ar *applicantRepo) Update(applicant *repo.Applicant) (*repo.Applicant, error) {
	query := `
		UPDATE applicants SET 
			first_name = $1,
			last_name = $2,
			speciality = $3,
			country = $4,
			city = $5,
			date_of_birth = $6,
			invisible_age = $7,
			phone_number = $8,
			about = $9,
			image_url = $10,
			updated_at = CURRENT_TIMESTAMP,
			slug = $11
		WHERE id = $12
		RETURNING email, last_step, created_at, updated_at
	`
	var (
		updatedAt sql.NullTime
	)
	err := ar.db.QueryRow(
		query,
		applicant.FirstName,
		applicant.LastName,
		applicant.Speciality,
		applicant.Country,
		applicant.City,
		applicant.DateOfBirth,
		applicant.InVisibleAge,
		applicant.PhoneNumber,
		applicant.About,
		applicant.ImageUrl,
		applicant.Slug,
		applicant.ID,
	).Scan(
		&applicant.Email,
		&applicant.LastStep,
		&applicant.CreatedAt,
		&updatedAt,
	)
	if err != nil {
		return nil, err
	}

	applicant.UpdatedAt = updatedAt.Time

	return applicant, nil
}

func (ar *applicantRepo) Delete(id int64) error {
	query := `
		DELETE FROM applicants WHERE id = $1
	`

	row, err := ar.db.Exec(
		query,
		id,
	)
	if err != nil {
		return err
	}

	if rowsAffected, _ := row.RowsAffected(); rowsAffected == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *applicantRepo) GetAll(params *repo.GetAllApplicantParams) (*repo.GetAllApplicants, error) {
	var (
		updatedAt sql.NullTime
	)
	result := repo.GetAllApplicants{
		Applicants: make([]*repo.Applicant, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := " WHERE true "
	for _, v := range params.Languages {
		filter += fmt.Sprintf(" AND a.id IN (SELECT l.applicant_id FROM languages l WHERE l.language LIKE '%s')", v)
	}
	for _, v := range params.Educations {
		filter += fmt.Sprintf(" AND a.id IN (SELECT e.applicant_id FROM educations e WHERE e.degree_level LIKE '%s')", v)
	}
	for _, v := range params.LicenceCertificates {
		filter += fmt.Sprintf(" AND a.id IN (SELECT c.applicant_id FROM licenses_and_sertificates c WHERE c.name LIKE '%s')", v)
	}
	for _, v := range params.Skills {
		filter += fmt.Sprintf(" AND a.id IN (SELECT s.applicant_id FROM skills s WHERE s.name LIKE '%s')", v)
	}

	query := `
		SELECT 
			a.id,
			a.first_name,
			a.last_name,
			a.speciality,
			a.country,
			a.city,
			a.date_of_birth,
			a.invisible_age,
			a.phone_number,
			a.email,
			a.about,
			a.image_url,
			a.last_step,
			a.created_at,
			a.updated_at,
			a.slug
		FROM applicants a
	` + filter + `
		ORDER BY a.created_at DESC
	` + limit

	rows, err := ur.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var applicant repo.Applicant
		err := rows.Scan(
			&applicant.ID,
			&applicant.FirstName,
			&applicant.LastName,
			&applicant.Speciality,
			&applicant.Country,
			&applicant.City,
			&applicant.DateOfBirth,
			&applicant.InVisibleAge,
			&applicant.PhoneNumber,
			&applicant.Email,
			&applicant.About,
			&applicant.ImageUrl,
			&applicant.LastStep,
			&applicant.CreatedAt,
			&updatedAt,
			&applicant.Slug,
		)
		if err != nil {
			return nil, err
		}

		applicant.UpdatedAt = updatedAt.Time
		result.Applicants = append(result.Applicants, &applicant)
	}

	queryCount := "SELECT count(1) FROM applicants a " + filter

	err = ur.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (ar *applicantRepo) UpdateStepField(arg string, email string) (*repo.Applicant, error) {
	query := `
		UPDATE applicants SET last_step = $1 WHERE  email = $2
		RETURNING 
			id,
			first_name,
			last_name,
			speciality,
			country,
			city,
			date_of_birth,
			invisible_age,
			phone_number,
			email,
			about,
			image_url,
			last_step,
			created_at,
			updated_at,
			slug
	`
	var (
		result    repo.Applicant
		updatedAt sql.NullTime
	)
	err := ar.db.QueryRow(
		query,
		arg,
		email,
	).Scan(
		&result.ID,
		&result.FirstName,
		&result.LastName,
		&result.Speciality,
		&result.Country,
		&result.City,
		&result.DateOfBirth,
		&result.InVisibleAge,
		&result.PhoneNumber,
		&result.Email,
		&result.About,
		&result.ImageUrl,
		&result.LastStep,
		&result.CreatedAt,
		&updatedAt,
		&result.Slug,
	)
	if err != nil {
		return nil, err
	}
	result.UpdatedAt = updatedAt.Time
	return &result, nil
}

func (ar *applicantRepo) UpdateEmail(newEmail string, id int64) error {
	query := `
		UPDATE applicants SET email = $1 WHERE id = $2
	`
	res, err := ar.db.Exec(
		query,
		newEmail,
		id,
	)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ar *applicantRepo) GetSlug(slug string) (int64, error) {
	var id int64
	query := fmt.Sprintf(" SELECT id FROM applicants WHERE slug = '%s' ", slug)
	err := ar.db.QueryRow(query).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}
