package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

func createSkill(t *testing.T) *repo.Skill {
	applicant := createApplicant(t)
	skill, err := dbManager.Skill().Create(&repo.Skill{
		ApplicantId: applicant.ID,
		Name:        faker.Word(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, skill)
	return skill
}

func deleteSkill(t *testing.T, applicantId int64) {
	err := dbManager.Skill().Delete(applicantId)
	require.NoError(t, err)
	deleteApplicant(t, applicantId)
}

func TestCreateSkill(t *testing.T) {
	skill := createSkill(t)
	deleteSkill(t, skill.ApplicantId)
}

func TestDeleteSkill(t *testing.T) {
	s1 := createSkill(t)
	deleteSkill(t, s1.ApplicantId)
}

func TestGetAllSkill(t *testing.T) {
	apids := []int64{}
	for i := 0; i < 10; i++ {
		s1 := createSkill(t)
		apids = append(apids, s1.ApplicantId)
	}
	skills, err := dbManager.Skill().GetAll(&repo.GetAllSkillParams{
		Limit: 10,
		Page:  1,
	})
	require.GreaterOrEqual(t, len(skills.Skills), 10)
	require.NoError(t, err)
	for i := 0; i < 10; i++ {
		deleteSkill(t, apids[i])
	}
}
