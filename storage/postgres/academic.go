package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/pkg/utils"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type academicRepo struct {
	db *sqlx.DB
}

func NewAcademic(db *sqlx.DB) repo.AcademicStorageI {
	return &academicRepo{
		db: db,
	}
}

func (ar *academicRepo) Create(a *repo.Academic) (*repo.Academic, error) {
	query := `
		INSERT INTO academic_test_results (
			applicant_id,
			test_name,
			organization,
			score,
			pdf_url
		) VALUES ($1, $2, $3,$4,$5)
		RETURNING id 
	`

	err := ar.db.QueryRow(
		query,
		a.ApplicantID,
		a.TestName,
		a.Organization,
		a.Score,
		utils.NullString(a.PdfUrl),
	).Scan(
		&a.ID,
	)
	if err != nil {
		return nil, err
	}

	return a, nil
}

func (ar *academicRepo) Get(id int64) (*repo.Academic, error) {
	var (
		academic repo.Academic
		pdfUrl   sql.NullString
	)
	query := `
		SELECT
			id,
			applicant_id,
			test_name,
			organization,
			score,
			pdf_url
		FROM academic_test_results 
		WHERE id = $1
	`
	err := ar.db.QueryRow(
		query,
		id,
	).Scan(
		&academic.ID,
		&academic.ApplicantID,
		&academic.TestName,
		&academic.Organization,
		&academic.Score,
		&pdfUrl,
	)
	if err != nil {
		return nil, err
	}
	academic.PdfUrl = pdfUrl.String

	return &academic, nil
}

func (ar *academicRepo) Update(a *repo.Academic) (*repo.Academic, error) {
	query := `
		UPDATE academic_test_results SET
			test_name = $1,
			organization = $2,
			score = $3,
			pdf_url = $4
	    WHERE id = $5 AND applicant_id = $6	
	`

	res, err := ar.db.Exec(
		query,
		a.TestName,
		a.Organization,
		a.Score,
		utils.NullString(a.PdfUrl),
		a.ID,
		a.ApplicantID,
	)
	if err != nil {
		return nil, err
	}
	if result, _ := res.RowsAffected(); result == 0 {
		return nil, sql.ErrNoRows
	}

	return a, nil
}

func (ar *academicRepo) Delete(id, applicantId int64) error {
	query := `
		DELETE FROM academic_test_results WHERE id = $1 and applicant_id = $2
	`

	result, err := ar.db.Exec(
		query,
		id,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ar *academicRepo) GetAll(params *repo.GetAllAcademicsParams) (*repo.GetAllAcademicResults, error) {
	result := repo.GetAllAcademicResults{
		Academics: make([]*repo.Academic, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			test_name,
			organization,
			score,
			pdf_url
		FROM academic_test_results 
	` + filter + limit

	rows, err := ar.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var pdfUrl sql.NullString
	for rows.Next() {
		var academic repo.Academic
		err := rows.Scan(
			&academic.ID,
			&academic.ApplicantID,
			&academic.TestName,
			&academic.Organization,
			&academic.Score,
			&pdfUrl,
		)
		if err != nil {
			return nil, err
		}
		academic.PdfUrl = pdfUrl.String

		result.Academics = append(result.Academics, &academic)
	}

	queryCount := "SELECT count(1) FROM academic_test_results " + filter

	err = ar.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
