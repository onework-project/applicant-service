package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/repo"
)

type languageRepo struct {
	db *sqlx.DB
}

func NewLanguage(db *sqlx.DB) repo.LanguageStorageI {
	return &languageRepo{
		db: db,
	}
}

func (lr *languageRepo) Create(l *repo.Language) (*repo.Language, error) {
	query := `
		INSERT INTO languages (
			applicant_id,
			language,
			level
		) VALUES ($1, $2, $3)
		RETURNING id 
	`

	err := lr.db.QueryRow(
		query,
		l.ApplicantId,
		l.Language,
		l.Level,
	).Scan(
		&l.ID,
	)
	if err != nil {
		return nil, err
	}

	return l, nil
}

func (lr *languageRepo) Delete(applicantId int64) error {
	query := `
		DELETE FROM languages WHERE applicant_id = $1
	`

	result, err := lr.db.Exec(
		query,
		applicantId,
	)
	if err != nil {
		return err
	}
	if res, _ := result.RowsAffected(); res == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (lr *languageRepo) GetAll(params *repo.GetAllLanguageParams) (*repo.GetAllLanguages, error) {
	result := repo.GetAllLanguages{
		Languages: make([]*repo.Language, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""
	if params.ApplicantID > 0 {
		filter = fmt.Sprintf(" WHERE applicant_id = %d ", params.ApplicantID)
	}

	query := `
		SELECT
			id,
			applicant_id,
			language,
			level
		FROM languages 
	` + filter + limit

	rows, err := lr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var language repo.Language
		err := rows.Scan(
			&language.ID,
			&language.ApplicantId,
			&language.Language,
			&language.Level,
		)
		if err != nil {
			return nil, err
		}

		result.Languages = append(result.Languages, &language)
	}

	queryCount := "SELECT count(1) FROM languages " + filter

	err = lr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
