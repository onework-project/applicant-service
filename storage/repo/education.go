package repo

type EducationStorageI interface {
	Create(w *Education) (*Education, error)
	Get(id int64) (*Education, error)
	Update(w *Education) (*Education, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllEduParams) (*GetAllEdus, error)
}

type Education struct {
	ID             int64
	ApplicantID    int64
	University     string
	DegreeName     string
	DegreeLevel    string
	StartYear      string
	EndYear        string
	ApplicantGrade int64
	MaxGrade       int64
	PdfUrl         string
}

type GetAllEduParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}

type GetAllEdus struct {
	Educations []*Education
	Count      int64
}
