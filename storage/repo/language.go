package repo

type LanguageStorageI interface {
	Create(u *Language) (*Language, error)
	Delete(applicant_id int64) error
	GetAll(params *GetAllLanguageParams) (*GetAllLanguages, error)
}

type Language struct {
	ID          int64
	ApplicantId int64
	Language    string
	Level       string
}

type GetAllLanguages struct {
	Languages []*Language
	Count     int64
}

type GetAllLanguageParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
