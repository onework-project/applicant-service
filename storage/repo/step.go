package repo

type StepStorageI interface {
	Create(applicant_id int64) (*Step, error)
	Get(applicant_id int64) (*Step, error)
	UpdateField(field string, arg bool, applicant_id int64) (*Step, error)
	Delete(applicant_id int64) error
}

type Step struct {
	ID                  int64
	ApplicantID         int64
	GeneralInfo         bool
	Work                bool
	Education           bool
	AcademicTests       bool
	LicenceCertificates bool
	Award               bool
	Research            bool
}
