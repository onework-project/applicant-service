package repo

type LicenseStorageI interface {
	Create(u *License) (*License, error)
	Get(id int64) (*License, error)
	Update(u *License) (*License, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllLicenseParams) (*GetAllLicenses, error)
}

type License struct {
	ID           int64
	ApplicantId  int64
	Name         string
	Organization string
	PdfUrl       string
}

type GetAllLicenses struct {
	Licenses []*License
	Count    int64
}

type GetAllLicenseParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
