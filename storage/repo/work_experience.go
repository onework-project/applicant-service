package repo

type WorkStorageI interface {
	Create(w *Work) (*Work, error)
	Get(id int64) (*Work, error)
	Update(w *Work) (*Work, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllWorkExpParams) (*GetAllWorkExps, error)
}

type Work struct {
	ID             int64
	ApplicantId    int64
	CompanyName    string
	WorkPosition   string
	CompanyWebsite string
	EmploymentType string
	StartYear      string
	StartMonth     string
	EndYear        string
	EndMonth       string
	Description    string
}

type GetAllWorkExpParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}

type GetAllWorkExps struct {
	WorkExps []*Work
	Count    int64
}
