package repo

import "time"

type SavedJobStorageI interface {
	Create(s *SavedJob) (*SavedJob, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllSavedJobsParams) (*GetAllSavedJobs, error)
}

type SavedJob struct {
	ID          int64
	ApplicantID int64
	VacancyID   int64
	CreatedAt   time.Time
}

type GetAllSavedJobs struct {
	SavedJobs []*SavedJob
	Count     int64
}

type GetAllSavedJobsParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
