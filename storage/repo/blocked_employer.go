package repo

import "time"

type BlockedCompanyStorageI interface {
	Create(s *BlockedCompany) (*BlockedCompany, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllBlockedCompaniesParams) (*GetAllBlockedCompanies, error)
}

type BlockedCompany struct {
	ID          int64
	ApplicantID int64
	CompanyID   int64
	CreatedAt   time.Time
}

type GetAllBlockedCompanies struct {
	BlockedCompanies []*BlockedCompany
	Count            int64
}

type GetAllBlockedCompaniesParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
