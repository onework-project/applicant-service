package repo

type ResearchStorageI interface {
	Create(w *Research) (*Research, error)
	Get(id int64) (*Research, error)
	Update(w *Research) (*Research, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllResearchParams) (*GetAllResearches, error)
}

type Research struct {
	ID               int64
	ApplicantID      int64
	OrganizationName string
	Position         string
	Description      string
	Supervisor       string
	SupervisorEmail  string
}

type GetAllResearchParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}

type GetAllResearches struct {
	Researches []*Research
	Count      int64
}
