package repo

type SkillStorageI interface {
	Create(u *Skill) (*Skill, error)
	Delete(applicant_id int64) error
	GetAll(params *GetAllSkillParams) (*GetAllSkills, error)
}

type Skill struct {
	ID          int64
	ApplicantId int64
	Name        string
}

type GetAllSkills struct {
	Skills []*Skill
	Count  int64
}

type GetAllSkillParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
