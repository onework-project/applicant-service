package repo

type AcademicStorageI interface {
	Create(u *Academic) (*Academic, error)
	Get(id int64) (*Academic, error)
	Update(u *Academic) (*Academic, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllAcademicsParams) (*GetAllAcademicResults, error)
}

type Academic struct {
	ID           int64
	ApplicantID  int64
	TestName     string
	Organization string
	Score        float64
	PdfUrl       string
}

type GetAllAcademicResults struct {
	Academics []*Academic
	Count     int64
}

type GetAllAcademicsParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
