package repo

import (
	"context"
	"time"
)

type ApplicantStorageI interface {
	Create(ctx context.Context, a *Applicant) (*Applicant, error)
	Get(id int64) (*Applicant, error)
	Update(a *Applicant) (*Applicant, error)
	Delete(id int64) error
	GetAll(p *GetAllApplicantParams) (*GetAllApplicants, error)
	UpdateStepField(arg string, email string) (*Applicant, error)
	UpdateEmail(newEmail string, id int64) error
	GetSlug(slug string) (int64, error)
}

type Applicant struct {
	ID           int64
	FirstName    string
	LastName     string
	Speciality   string
	Country      string
	City         string
	DateOfBirth  string
	InVisibleAge bool
	PhoneNumber  string
	Email        string
	About        string
	ImageUrl     string
	LastStep     string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	Slug         string
}

type GetAllApplicantParams struct {
	Limit               int64
	Page                int64
	Languages           []string
	Educations          []string
	LicenceCertificates []string
	Skills              []string
}

type GetAllApplicants struct {
	Applicants []*Applicant
	Count      int64
}
