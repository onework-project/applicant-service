package repo

type SocialMediaStorageI interface {
	Create(u *SocialMedia) (*SocialMedia, error)
	Delete(applicant_id int64) error
	GetAll(params *GetAllSocialMediasParams) (*GetAllSocialMedias, error)
}

type SocialMedia struct {
	ID          int64
	ApplicantId int64
	Name        string
	Url         string
}

type GetAllSocialMedias struct {
	SocialMedias []*SocialMedia
	Count        int64
}

type GetAllSocialMediasParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
