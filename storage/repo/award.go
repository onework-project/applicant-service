package repo

type AwardStorageI interface {
	Create(u *Award) (*Award, error)
	Get(id int64) (*Award, error)
	Update(u *Award) (*Award, error)
	Delete(id, applicant_id int64) error
	GetAll(params *GetAllAwardParams) (*GetAllAwards, error)
}

type Award struct {
	ID           int64
	ApplicantId  int64
	AwardName    string
	Organization string
	PdfUrl       string
}

type GetAllAwards struct {
	Awards []*Award
	Count  int64
}

type GetAllAwardParams struct {
	Limit       int64
	Page        int64
	ApplicantID int64
}
