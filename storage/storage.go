package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/applicant-service/storage/postgres"
	"gitlab.com/onework-project/applicant-service/storage/repo"
	"go.opentelemetry.io/otel/trace"
)

type StorageI interface {
	Applicant() repo.ApplicantStorageI
	SocialMedia() repo.SocialMediaStorageI
	Work() repo.WorkStorageI
	Education() repo.EducationStorageI
	Academic() repo.AcademicStorageI
	Language() repo.LanguageStorageI
	Skill() repo.SkillStorageI
	License() repo.LicenseStorageI
	Research() repo.ResearchStorageI
	Award() repo.AwardStorageI
	SavedJob() repo.SavedJobStorageI
	BlockedCompany() repo.BlockedCompanyStorageI
	Step() repo.StepStorageI
}

type StoragePg struct {
	applicantRepo      repo.ApplicantStorageI
	socialMediaRepo    repo.SocialMediaStorageI
	workRepo           repo.WorkStorageI
	educationRepo      repo.EducationStorageI
	academicRepo       repo.AcademicStorageI
	languageRepo       repo.LanguageStorageI
	skillRepo          repo.SkillStorageI
	licenseRepo        repo.LicenseStorageI
	researchRepo       repo.ResearchStorageI
	awardRepo          repo.AwardStorageI
	savedJobRepo       repo.SavedJobStorageI
	blockedCompanyRepo repo.BlockedCompanyStorageI
	stepRepo           repo.StepStorageI
}

func NewStoragePg(db *sqlx.DB, tracer trace.Tracer) StorageI {
	return &StoragePg{
		applicantRepo:      postgres.NewApplicant(db, tracer),
		socialMediaRepo:    postgres.NewSocialMedia(db),
		workRepo:           postgres.NewWork(db),
		educationRepo:      postgres.NewEducation(db),
		academicRepo:       postgres.NewAcademic(db),
		languageRepo:       postgres.NewLanguage(db),
		skillRepo:          postgres.NewSkill(db),
		licenseRepo:        postgres.NewLicense(db),
		researchRepo:       postgres.NewResearch(db),
		awardRepo:          postgres.NewAward(db),
		savedJobRepo:       postgres.NewSavedJob(db),
		blockedCompanyRepo: postgres.NewBlockedCompany(db),
		stepRepo:           postgres.NewStep(db),
	}
}

func (s *StoragePg) Applicant() repo.ApplicantStorageI {
	return s.applicantRepo
}

func (s *StoragePg) SocialMedia() repo.SocialMediaStorageI {
	return s.socialMediaRepo
}

func (s *StoragePg) Work() repo.WorkStorageI {
	return s.workRepo
}

func (s *StoragePg) Education() repo.EducationStorageI {
	return s.educationRepo
}

func (s *StoragePg) Academic() repo.AcademicStorageI {
	return s.academicRepo
}

func (s *StoragePg) Language() repo.LanguageStorageI {
	return s.languageRepo
}

func (s *StoragePg) Skill() repo.SkillStorageI {
	return s.skillRepo
}

func (s *StoragePg) License() repo.LicenseStorageI {
	return s.licenseRepo
}

func (s *StoragePg) Research() repo.ResearchStorageI {
	return s.researchRepo
}

func (s *StoragePg) Award() repo.AwardStorageI {
	return s.awardRepo
}

func (s *StoragePg) SavedJob() repo.SavedJobStorageI {
	return s.savedJobRepo
}

func (s *StoragePg) BlockedCompany() repo.BlockedCompanyStorageI {
	return s.blockedCompanyRepo
}

func (s *StoragePg) Step() repo.StepStorageI {
	return s.stepRepo
}
