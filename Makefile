-include .env
.SILENT:
CURRENT_DIR=$(shell pwd)
DB_URL=postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=disable

run:
	go run cmd/main.go

tidy:
	go mod tidy
	go mod vendor

local:
	docker compose -f docker-compose.yml up --build -d

down:
	docker compose down

migrateup:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrateup1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migratedown:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migratedown1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1

proto-gen:
	rm -rf genproto
	./scripts/gen-proto.sh ${CURRENT_DIR}

unit-test:
	go test -v -cover ./storage/postgres/...

pull-sub-module:
	git submodule update --init --recursive

update-sub-module:
	git submodule update --remote --merge 

lint:
	golangci-lint run ./...

cache:
	go clean -testcache
