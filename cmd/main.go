package main

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/onework-project/applicant-service/config"
	pb "gitlab.com/onework-project/applicant-service/genproto/applicant_service"
	grpcPkg "gitlab.com/onework-project/applicant-service/pkg/grpc_client"
	"gitlab.com/onework-project/applicant-service/pkg/jaeger"
	"gitlab.com/onework-project/applicant-service/pkg/logging"
	"gitlab.com/onework-project/applicant-service/service"
	"gitlab.com/onework-project/applicant-service/storage"

	"go.opentelemetry.io/otel"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load(".")

	logging.Init()
	log := logging.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)

	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	tp, err := jaeger.InitJaeger(&cfg)
	if err != nil {
		log.Fatal("cannot create tracer", err)
	}
	defer tp.Shutdown(context.Background())

	grpcCon, err := grpcPkg.New(&cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v\n", err)
	}

	otel.SetTracerProvider(tp)
	tr := tp.Tracer("main-otel")
	log.Info("Opentracing connected")

	strg := storage.NewStoragePg(psqlConn, tr)

	applicantService := service.NewApplicantService(strg, &log, grpcCon, tr)
	academicService := service.NewAcademicService(strg, &log)
	awardService := service.NewAwardService(strg, &log)
	educationService := service.NewEducationService(strg, &log)
	languageService := service.NewLanguageService(strg, &log)
	licenseService := service.NewLicenseService(strg, &log)
	researchService := service.NewResearchService(strg, &log)
	skillService := service.NewSkillService(strg, &log)
	socialMediaService := service.NewSocialMediaService(strg, &log)
	workExpService := service.NewWorkExpService(strg, &log)
	blockedCompany := service.NewBlockedCompany(strg, &log, grpcCon)
	savedJob := service.NewSavedJob(strg, &log, grpcCon)
	step := service.NewStepService(strg, &log, tr)

	listen, err := net.Listen("tcp", cfg.GrpcPort)

	s := grpc.NewServer()

	pb.RegisterApplicantServiceServer(s, applicantService)
	pb.RegisterAcademicServiceServer(s, academicService)
	pb.RegisterAwardServiceServer(s, awardService)
	pb.RegisterEducationServiceServer(s, educationService)
	pb.RegisterLanguageServiceServer(s, languageService)
	pb.RegisterLicenseServiceServer(s, licenseService)
	pb.RegisterResearchServiceServer(s, researchService)
	pb.RegisterSkillServiceServer(s, skillService)
	pb.RegisterSocialMediaServiceServer(s, socialMediaService)
	pb.RegisterWorkExpServiceServer(s, workExpService)
	pb.RegisterBlockedCompanyServiceServer(s, blockedCompany)
	pb.RegisterSavedJobServiceServer(s, savedJob)
	pb.RegisterStepServiceServer(s, step)

	reflection.Register(s)

	log.Info("gRPC server started port in: ", cfg.GrpcPort)
	if s.Serve(listen); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
