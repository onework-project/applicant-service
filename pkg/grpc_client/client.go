package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/applicant-service/config"
	pbc "gitlab.com/onework-project/applicant-service/genproto/company_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	CompanyService() pbc.CompanyServiceClient
	VacancyService() pbc.VacancyServiceClient
	ApplicationService() pbc.ApplicationServiceClient
}

type GrpcClient struct {
	cfg         *config.Config
	connections map[string]interface{}
}

func New(cfg *config.Config) (GrpcClientI, error) {
	conCompanyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("company service dial host: %s port %s err: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort, err)
	}
	conVacancyService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("vacancy service dial host: %s port %s err: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort, err)
	}
	conApplication, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("application dial host: %s port %s err: %v", cfg.CompanyServiceHost, cfg.CompanyServiceGrpcPort, err)
	}
	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"company_service":     pbc.NewCompanyServiceClient(conCompanyService),
			"vacancy_service":     pbc.NewVacancyServiceClient(conVacancyService),
			"application_service": pbc.NewApplicationServiceClient(conApplication),
		},
	}, nil
}

func (g *GrpcClient) CompanyService() pbc.CompanyServiceClient {
	return g.connections["company_service"].(pbc.CompanyServiceClient)
}

func (g *GrpcClient) VacancyService() pbc.VacancyServiceClient {
	return g.connections["vacancy_service"].(pbc.VacancyServiceClient)
}

func (g *GrpcClient) ApplicationService() pbc.ApplicationServiceClient {
	return g.connections["application_service"].(pbc.ApplicationServiceClient)
}
